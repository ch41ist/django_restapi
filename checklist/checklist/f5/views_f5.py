from django.shortcuts import render
import mysql.connector
from datetime import date
from checklist.f5.models import f5Fan, f5PSU, f5MB
import netmiko
import json
from datetime import datetime, timedelta
import time, threading
import schedule
import time
import mysql.connector
import datetime
import subprocess
import pandas as pd

db = mysql.connector.connect(
    host = 'localhost',
    user = 'root',
    password = 'xcalculusx',
    database = 'checklistdata'
)

daytime = datetime.datetime.now()

def f5(request):
    if request.method == "POST":
        f5collectdata()

    f5fan = f5Fan.objects.filter().order_by('-id')
    f5psu = f5PSU.objects.filter().order_by('-id')
    f5mb = f5MB.objects.filter().order_by('-id')
    
    context = {
        'f5fan': f5fan,
        'f5psu': f5psu,
        'f5mb': f5mb
    }
    return render(request,'f5datatable.html', context)

def f5collectdata():
    devices = """
        10.182.0.45
        10.182.0.46
        10.182.0.47
        10.150.4.124
        10.150.4.125
        10.150.4.126
        10.150.4.127
        10.150.4.128
        10.150.4.129
        10.150.4.130
        """.strip().splitlines()
    device_type = 'f5_ltm'
    username = 'admin'
    password = 'vrngo'

    netmiko_exceptions = (netmiko.ssh_exception.NetMikoTimeoutException,
                          netmiko.ssh_exception.NetMikoAuthenticationException)
    
    for device in devices:
        try:

            print('~' * 79)
            print('connecting to ', device)
            connection = netmiko.BaseConnection(ip=device, device_type=device_type, username=username, password=password)

            sendCommand = connection.send_command('tmsh show sys hardware')
            listofdata = sendCommand
            datalist = listofdata.splitlines()
            
            summData = pd.DataFrame(datalist)
            fans = summData[4:8]
            
            decodefan = str(fans).split()

            #numberfan1 = decodefan[2]
            fanstatus1 = decodefan[3]
            fanspeed1 = decodefan[5]

            #numberfan2 = decodefan[7]
            fanstatus2 = decodefan[8]
            fanspeed2 = decodefan[10]

            #numberfan3 = decodefan[12]
            fanstatus3 = decodefan[13]
            fanspeed3 = decodefan[15]

            #numberfan4 = decodefan[17]
            fanstatus4 = decodefan[18]
            fanspeed4 = decodefan[20]

            

           #print('number fan: '+numberfan1+ ' /status fan: '+fanstatus1+ ' /fan speed: '+fanspeed1)
            #print('number fan: '+numberfan2+ ' /status fan: '+fanstatus2+ ' /fan speed: '+fanspeed2)
            #print('number fan: '+numberfan3+ ' /status fan: '+fanstatus3+ ' /fan speed: '+fanspeed3)
            #print('number fan: '+numberfan4+ ' /status fan: '+fanstatus4+ ' /fan speed: '+fanspeed4)


            psu = summData[17:19]
            datapsu = str(psu).split()

            psunumb1 = datapsu[2]
            psustatus1 = datapsu[3]
            psucurrenttype1 = datapsu[4]

            psunumb2 = datapsu[6]
            psustatus2 = datapsu[7]
            psucurrenttype2 = datapsu[8]
            print('psu number: '+psunumb1 + ' /psu status: '+ psustatus1 + ' /psu current type: '+psucurrenttype1)
            print('psu number: '+psunumb2 + ' /psu status: '+ psustatus2 + ' /psu current type: '+psucurrenttype2)


            temperate = summData[22:27]
            
            datatemp = str(temperate).split()

            tempinletlow = datatemp[3]
            tempinletcurrent = datatemp[4]
            tempinlethight = datatemp[5]

            tempoutletlow = datatemp[9]
            tempoutletcurrent = datatemp[10]
            tempoutlethight = datatemp[11]

            temppsu1low = datatemp[15]
            temppsu1current = datatemp[16]
            temppsu1hight = datatemp[17]

            temppsu2low = datatemp[21]
            temppsu2current = datatemp[22]
            temppsu2hight = datatemp[23]

            tempMBlow = datatemp[27]
            tempMBcurrent = datatemp[28]
            tempMBhight = datatemp[29]

            #print(temperate)
            print('temp inlet low : '+ tempinletlow+ ' /temp inlet current: '+tempinletcurrent+ ' /temp inlet hight: '+tempinlethight)
            print('temp outlet low : '+ tempoutletlow+ ' /temp outlet current: '+tempoutletcurrent+ ' /temp outlet hight: '+tempoutlethight)
            print('temp psu1 low : '+ temppsu1low+ ' /temp psu1 current: '+temppsu1current+ ' /temp psu1 hight: '+temppsu1hight)
            print('temp psu2 low : '+ temppsu2low+ ' /temp psu2 current: '+temppsu2current+ ' /temp psu2 hight: '+temppsu2hight)
            print('temp mainboard low : '+ tempMBlow+ ' /temp mainboard current: '+tempMBcurrent+ ' /temp mainboard hight: '+tempMBhight)


            cpu = summData[30:31]
            cpudata = str(cpu).split()

            cputemp = cpudata[3]
            cpufanspeed = cpudata[4]
            print('cpu temperate: '+cputemp+' /cpu fan speed: '+cpufanspeed)

            
            cursor = db.cursor()
            sqlmb = """ insert into f5_f5mb (ip, tempinletlow, tempinletcurrent, tempinlethight, tempoutletlow,
                                            tempoutletcurrent, tempoutlethight, tempMBlow, tempMBcurrent,
                                            tempMBhight, tempcpu, cpufanspeed, created, updated
                                            )
                    VALUES ( %s, %s, %s, %s, %s, 
                            %s, %s, %s, %s, %s, 
                            %s, %s, %s, %s
                            ) """
            valuesmb = (device,  tempinletlow, tempinletcurrent, tempinlethight, tempoutletlow,
                        tempoutletcurrent, tempoutlethight, tempMBlow, tempMBcurrent, tempMBhight, 
                        cputemp, cpufanspeed, daytime, daytime)
            cursor.execute(sqlmb, valuesmb)

            sqlpsu = """ insert into f5_f5psu (
                        ip, psunumber1, psunumber2, psustatus1, psustatus2,
                        psu1currenttype, psu2currenttype, temppsu1low, temppsu1current, temppsu1hight,
                        temppsu2low, temppsu2current, temppsu2hight, created, updated)
                        VALUES ( %s, %s, %s, %s, %s, 
                                %s, %s, %s, %s, %s, 
                                %s, %s, %s, %s, %s
                                ) """
            valuespsu = (device, psunumb1, psunumb2, psustatus1, psustatus2, 
                         psucurrenttype1, psucurrenttype2, temppsu1low, temppsu1current, temppsu1hight,
                         temppsu2low, temppsu2current, temppsu2hight, daytime, daytime)
            cursor.execute(sqlpsu, valuespsu)

            sqlfan = """insert into f5_f5fan (
                        ip, fanstatus1, fanstatus2, fanstatus3, fanstatus4, 
                        fanspeed1, fanspeed2, fanspeed3, fanspeed4, created, 
                        updated )
                        VALUES ( %s, %s, %s, %s, %s, 
                                %s, %s, %s, %s, %s, 
                                %s
                                ) """
            valuesfan = (device,  fanstatus1, fanstatus2, fanstatus3, fanstatus4,
                        fanspeed1, fanspeed2, fanspeed3, fanspeed4, daytime, daytime
                        )
            cursor.execute(sqlfan, valuesfan)
            db.commit()
            print('process: F5 ทำงานเสร็จสิ้น...')

        except netmiko_exceptions as e:
            print('enable to connect to ',devices, e)