# Generated by Django 2.2.6 on 2019-11-06 08:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('f5', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='f5data',
            name='descriptions',
        ),
        migrations.RemoveField(
            model_name='f5data',
            name='workname',
        ),
    ]
