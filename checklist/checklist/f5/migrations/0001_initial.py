# Generated by Django 2.2.6 on 2019-11-06 04:47

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='f5data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('workname', models.CharField(default='', max_length=1000)),
                ('descriptions', models.CharField(default='', max_length=1000)),
                ('ip', models.CharField(default='', max_length=1000)),
                ('fannumber1', models.CharField(default='', max_length=100)),
                ('fannumber2', models.CharField(default='', max_length=100)),
                ('fannumber3', models.CharField(default='', max_length=100)),
                ('fannumber4', models.CharField(default='', max_length=100)),
                ('fanstatus1', models.CharField(default='', max_length=100)),
                ('fanstatus2', models.CharField(default='', max_length=100)),
                ('fanstatus3', models.CharField(default='', max_length=100)),
                ('fanstatus4', models.CharField(default='', max_length=100)),
                ('fanspeed1', models.CharField(default='', max_length=100)),
                ('fanspeed2', models.CharField(default='', max_length=100)),
                ('fanspeed3', models.CharField(default='', max_length=100)),
                ('fanspeed4', models.CharField(default='', max_length=100)),
                ('psunumber1', models.CharField(default='', max_length=100)),
                ('psunumber2', models.CharField(default='', max_length=100)),
                ('psustatus1', models.CharField(default='', max_length=100)),
                ('psustatus2', models.CharField(default='', max_length=100)),
                ('psuspeed1', models.CharField(default='', max_length=100)),
                ('psuspeed2', models.CharField(default='', max_length=100)),
                ('tempinletlow', models.CharField(default='', max_length=100)),
                ('tempinletcurrent', models.CharField(default='', max_length=100)),
                ('tempinlethight', models.CharField(default='', max_length=100)),
                ('tempoutletlow', models.CharField(default='', max_length=100)),
                ('tempoutletcurrent', models.CharField(default='', max_length=100)),
                ('tempoutlethight', models.CharField(default='', max_length=100)),
                ('temppsu1low', models.CharField(default='', max_length=100)),
                ('temppsu1current', models.CharField(default='', max_length=100)),
                ('temppsu1hight', models.CharField(default='', max_length=100)),
                ('temppsu2low', models.CharField(default='', max_length=100)),
                ('temppsu2current', models.CharField(default='', max_length=100)),
                ('temppsu2hight', models.CharField(default='', max_length=100)),
                ('tempMBlow', models.CharField(default='', max_length=100)),
                ('tempMBcurrent', models.CharField(default='', max_length=100)),
                ('tempMBhight', models.CharField(default='', max_length=100)),
                ('tempcpu', models.CharField(default='', max_length=100)),
                ('cpufanspeed', models.CharField(default='', max_length=100)),
                ('created', models.DateField(auto_now_add=True, null=True)),
                ('updated', models.DateField(auto_now=True, null=True)),
            ],
        ),
    ]
