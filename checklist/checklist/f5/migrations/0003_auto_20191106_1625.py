# Generated by Django 2.2.6 on 2019-11-06 09:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('f5', '0002_auto_20191106_1543'),
    ]

    operations = [
        migrations.RenameField(
            model_name='f5data',
            old_name='psuspeed1',
            new_name='psu1currenttype',
        ),
        migrations.RenameField(
            model_name='f5data',
            old_name='psuspeed2',
            new_name='psu2currenttype',
        ),
    ]
