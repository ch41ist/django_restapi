from django.db import models
import datetime
from datetime import datetime
from django.utils import timezone

class f5Fan(models.Model):
    ip = models.CharField(max_length=1000, default="")
    fanstatus1 = models.CharField(max_length=100, default="")
    fanstatus2 = models.CharField(max_length=100, default="")
    fanstatus3 = models.CharField(max_length=100, default="")
    fanstatus4 = models.CharField(max_length=100, default="")
    fanspeed1 = models.CharField(max_length=100, default="")
    fanspeed2 = models.CharField(max_length=100, default="")
    fanspeed3 = models.CharField(max_length=100, default="")
    fanspeed4 = models.CharField(max_length=100, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class f5PSU(models.Model):
    ip = models.CharField(max_length=1000, default="")
    psunumber1 = models.CharField(max_length=100, default="")
    psustatus1 = models.CharField(max_length=100, default="")
    psu1currenttype = models.CharField(max_length=100, default="")
    temppsu1low = models.CharField(max_length=100, default="")
    temppsu1current = models.CharField(max_length=100, default="")
    temppsu1hight = models.CharField(max_length=100, default="")
    
    psustatus2 = models.CharField(max_length=100, default="")
    psunumber2 = models.CharField(max_length=100, default="") 
    psu2currenttype = models.CharField(max_length=100, default="")
    temppsu2low = models.CharField(max_length=100, default="")
    temppsu2current = models.CharField(max_length=100, default="")
    temppsu2hight = models.CharField(max_length=100, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class f5MB(models.Model):
    ip = models.CharField(max_length=1000, default="") 
    tempinletlow = models.CharField(max_length=100, default="")
    tempinletcurrent = models.CharField(max_length=100, default="")
    tempinlethight = models.CharField(max_length=100, default="")

    tempoutletlow = models.CharField(max_length=100, default="")
    tempoutletcurrent = models.CharField(max_length=100, default="")
    tempoutlethight = models.CharField(max_length=100, default="")
    
    tempMBlow = models.CharField(max_length=100, default="")
    tempMBcurrent = models.CharField(max_length=100, default="")
    tempMBhight = models.CharField(max_length=100, default="")
    tempcpu = models.CharField(max_length=100, default="")
    cpufanspeed = models.CharField(max_length=100, default="")

    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)
