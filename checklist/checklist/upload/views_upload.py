from django.shortcuts import render

from django.conf import settings
from django.core.files.storage import FileSystemStorage

# Create your views here.

def upload(request):
    if request.method == 'POST':
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        fs.save(myfile.name, myfile)
    return render(request, 'upload.html')