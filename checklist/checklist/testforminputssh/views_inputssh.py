from django.shortcuts import render

from .forms import CmdForm
# Create your views here.
def index(request):
        if request.method == "POST":
                form = CmdForm(request.POST)
                if form.is_valid():
                        from netmiko import ConnectHandler
                        device = {}
                        device['device_type'] = 'cisco_ios'
                        device['ip'] = '110.170.246.201'
                        device['username'] = request.POST.get('username')
                        device['password'] = request.POST.get('passwords')
                        cmd = 'service asterisk status | grep Active: && service httpd status | grep Active: '
                        conn = ConnectHandler(**device)
                        output = conn.send_command(cmd)
                        return render(request, 'index2.html', {'form': form, 'output': output})
        else:
                form = CmdForm()
                return render(request, 'index2.html', {'form': form})
