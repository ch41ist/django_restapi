"""checklist URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from checklist.dashboard import views
from checklist.voice import views_voice
from checklist.apradioreset import views_ap
from checklist.f5 import views_f5
from checklist.routerandswitch import views_router
from checklist.sdwan import views_sdwan
from checklist.wirelessoffice import views_wireless
# from checklist.testjson import views_json
from checklist.testssh import views_ssh
#from checklist.voice import views_voice_overs
from checklist.testforminputssh import views_inputssh
# from checklist.testhtml import views_test
from checklist.voice import view_voice_auto, views_voice_data_table, view_voice_M
from checklist.wirelessoffice import views_wireless, views_health, views_wireless_dashboard
from checklist.upload import views_upload
from checklist.apradioreset import views_ap

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('ex1/', include('checklist.testjson.urls')),
    path('', views.dashboard, name='dashboard'),
    path('voice/', views_voice.voice, name='voice'),
    path('ap/', views_ap.ap, name='ap'),
    path('f5/', views_f5.f5, name='f5'),
    path('router/', views_router.router, name='router'),
    path('sdwan/', views_sdwan.sdwan, name='sdwan'),
    path('wireless/', views_wireless.wireless, name='wireless'),
    path('ssh/', views_ssh.index, name='ssh'),
    path('inputssh/', views_inputssh.index, name='inputssh'),
    path('voiceAuto/', view_voice_auto.voiceAuto, name='voiceAuto'),
    path('voicedataTable/', views_voice_data_table.dataTable, name='voicedataTable'),
    path('voicemanual/', view_voice_M.manual, name='voicemanual'),
    path('wireless/', views_wireless.wireless, name='wireless'),
    path('controllerhealth/', views_health.health, name='health'),
    path('upload/', views_upload.upload, name='upload'),
    path('controllerAP/', views_ap.ap, name='controllerAP' ),
    path('wireless_dashboard', views_wireless_dashboard.wireless_dashboard, name="wireless_dashboard"),
    # path('html', views_test.test, name='test'),
    #path('data/', views_voice_overs.voicedata, name='data'),
    
]


from django.conf import settings
from django.conf.urls.static import static

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)