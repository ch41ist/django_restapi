from django.shortcuts import render
from checklist.wirelessoffice.models import wireofficeDown, controllerhealthCPU, controllerhealthRAM, countap

def ap(request):
    # healthcpu = controllerhealthCPU.objects.filter().order_by('-id')[:5]
    # healthram = controllerhealthRAM.objects.filter().order_by('-id')[:5]
    countAP  = countap.objects.filter().order_by('-id')[:5]
    context = {'countAP' : countAP,
                
    
    }
    return render(request,'controllerAP.html', context)
