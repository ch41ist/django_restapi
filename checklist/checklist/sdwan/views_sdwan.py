from django.shortcuts import render
from selenium import webdriver 
from time import sleep 
import mysql.connector
from datetime import date
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from checklist.sdwan.models import edges, links, alldata, dataalert
import email
import getpass, imaplib
import os
import sys
from zipfile import ZipFile
import time

import glob
import csv
from itertools import islice
import pandas as pd
from datetime import datetime, timedelta, date
import fnmatch
import psutil
import mysql.connector
import pandas as pd
from django.core.files import File


now = datetime.now()
date_time = now.strftime("%m_%d_%Y")
daytime = datetime.today()

db = mysql.connector.connect(
    host = 'localhost',
    user = 'root',
    password = 'xcalculusx',
    database = 'checklistdata'
)


def sdwan(request):
    edge = edges.objects.filter().order_by('-id')[0]
    link = links.objects.filter().order_by('-id')[0]
    allstate = alldata.objects.filter().order_by('-id')[:43]
    dataAlert = dataalert.objects.filter().latest('id')
    # allr = alldata.objects.filter().order_by('-id')[0]
    # print(allr)
    context = {
        'edge': edge,
        'link': link,
        'allstate': allstate,
        'dataAlert': dataAlert,
        
    }
    if request.method == "POST":
        collectFile()
        sleep(1)
        collectFile2()
        sleep(1)
        processfiles()
        sleep(1)
        processalerts()
    
    #context = {'data': 'links[0]'}
    return render(request,'sdwan.html', context)

def collectFile():
    #
    
    try:
        option = webdriver.ChromeOptions()
        pre = {'download.default_directory':'/home/systemx/BitGitDJANGO/django_restapi/checklist'}
        option.add_experimental_option('prefs', pre)
        #option.add_argument('download.default_directory=/home/systemx/BitGitDJANGO/django_restapi/checklist/checklist/sdwan/getfiles')
        usr='maneeratmua@gosoft.co.th' 
        pwd='12345'  

        driver = webdriver.Chrome('/home/systemx/BitGitDJANGO/django_restapi/checklist/driver/chromedriver_linux64/chromedriver', chrome_options=option) 
        #driver = webdriver.Chrome(chrome_options=option)
        driver.get('https://vco3-sin1.velocloud.net/') 
        print ("Opened VeloCould") 
        sleep(1) 
        
        username_box = driver.find_element_by_id('username') 
        username_box.send_keys(usr) 
        print ("Email Id entered") 
        sleep(1) 
        
        password_box = driver.find_element_by_id('password') 
        password_box.send_keys(pwd) 
        print ("Password entered") 
        print("เข้าไปละเหอะ")
        
        login_box = driver.find_element_by_id('btnSubmit') 
        login_box.click()
        # //*[@id="nav-monitor-edges"]
        sleep(3)
        
        ###ไปดึงข้อมูลจาก web velocloud โดยการค้นหา element id บนหน้านั้นๆ แล้วนำไปเก็บเป็นข้อมูลมาใช้งานบนแดชบอร์ด###
        seedata = driver.find_elements_by_id('edges')
        edgeslist = []
        edges = [t.text for t in seedata]
        for idx in edges:
            ap = idx.split()
            edgeslist.append(ap)
        cursor = db.cursor()
        sqledges = """
                insert into sdwan_edges 
                (down, degraded, connected, outOfActivated, created, updated)
                values (%s, %s, %s, %s, %s, %s)
        """
        valuesedges = (edgeslist[0][5], edgeslist[0][7], edgeslist[0][9], edgeslist[0][0], daytime, daytime)
        #print(valuesedges)

        cursor.execute(sqledges, valuesedges)
        #db.commit()
        ###ไปดึงข้อมูลจาก web velocloud โดยการค้นหา element id บนหน้านั้นๆ แล้วนำไปเก็บเป็นข้อมูลมาใช้งานบนแดชบอร์ด###

        ###ไปดึงข้อมูลจาก web velocloud โดยการค้นหา element id บนหน้านั้นๆ แล้วนำไปเก็บเป็นข้อมูลมาใช้งานบนแดชบอร์ด###
        collectingLink = driver.find_elements_by_id('links')
        linklist = []
        links = [data.text for data in collectingLink]
        for idxlink in links:
            aplink = idxlink.split()
            linklist.append(aplink)
        cursor = db.cursor()
        sqlllinks = """
                insert into sdwan_links 
                (down, unstable, stable, alllinks, created, updated)
                values (%s, %s, %s, %s, %s, %s)
        """
        #print(linklist)
        valueslinks = (linklist[0][1], linklist[0][3], linklist[0][5], linklist[0][0], daytime, daytime)
        cursor.execute(sqlllinks, valueslinks)
        db.commit()
        ###ไปดึงข้อมูลจาก web velocloud โดยการค้นหา element id บนหน้านั้นๆ แล้วนำไปเก็บเป็นข้อมูลมาใช้งานบนแดชบอร์ด###
        sleep(3)
        

        ####this path use for download files from web velocloud####
        get_file1 = driver.find_element_by_id('nav-monitor-edges')
        get_file1.click()
        sleep(3)
        get_filew = driver.find_element_by_id('grdToolbarExportCsv')
        get_filew.click()
        sleep(2)
        load_file = driver.find_element_by_id('btnDownloadCsv')
        load_file.click()
        sleep(2)
        # outc = driver.find_element_by_id('btnCancel')
        # outc.click()
        # sleep(1)
        ####this path use for download files from web velocloud####

        ####this path use for download files from web velocloud####
        
        ####this path use for download files from web velocloud####

        print ("Done")
        driver.quit()  

    except IOError as e:
        print(e)

def collectFile2():
    
    try:
        option = webdriver.ChromeOptions()
        pre = {'download.default_directory':'/home/systemx/BitGitDJANGO/django_restapi/checklist'}
        option.add_experimental_option('prefs', pre)
        #option.add_argument('download.default_directory=/home/systemx/BitGitDJANGO/django_restapi/checklist/checklist/sdwan/getfiles')
        usr='maneeratmua@gosoft.co.th' 
        pwd='12345'  

        driver = webdriver.Chrome('/home/systemx/Documents/chromedriver_linux64/chromedriver', chrome_options=option) 
        #driver = webdriver.Chrome(chrome_options=option)
        driver.get('https://vco3-sin1.velocloud.net/') 
        print ("Opened VeloCould") 
        sleep(1) 
        
        username_box = driver.find_element_by_id('username') 
        username_box.send_keys(usr) 
        print ("Email Id entered") 
        sleep(1) 
        
        password_box = driver.find_element_by_id('password') 
        password_box.send_keys(pwd) 
        print ("Password entered") 
        print("เข้าไปละเหอะ")
        
        login_box = driver.find_element_by_id('btnSubmit') 
        login_box.click()
        # //*[@id="nav-monitor-edges"]
        sleep(3)
        
        ####this path use for download files from web velocloud####
        get_file2 = driver.find_element_by_id('nav-monitor-alerts')
        get_file2.click()
        sleep(3)
        get_filer = driver.find_element_by_id('grdToolbarExportCsv')
        get_filer.click()
        sleep(2)
        send = driver.find_element_by_id('csvFileName')
        send.clear()
        send.send_keys("alert.csv")
        sleep(2)
        load_file = driver.find_element_by_id('btnDownloadCsv')
        load_file.click()
        sleep(2)
        # oute = driver.find_element_by_id('btnCancel')
        # oute.click()
        # sleep(1)
        ####this path use for download files from web velocloud####

        print ("Done")
        driver.quit()  

    except IOError as e:
        print(e)


def processfiles():
    
            #filename  = os.path.expanduser('/home/systemx/BitGitDJANGO/django_restapi/checklist/attachments/*.csv') 
            try:
                ###processing files after unzip and make new concate files that collect from source file###
                pathsRead = '/home/systemx/BitGitDJANGO/django_restapi/checklist'
                ros = []
                listof = os.listdir(pathsRead)
                pattern = 'export*.csv'
                count = 0
                for e in listof:
                    count = count+1
                    
                    if fnmatch.fnmatch(e, pattern):
                        print(e)
                        with open(e, 'r') as f:
                            csv_read = csv.reader(f)
                            listrow = list(csv_read)
                            lenrow = len(listrow)
                            #ros.append(['Edge',	'Status', 'HA',	'Connected Links',	'Degraded Links', 'Disconnected Links',	'Other Link States','Cloud Services Status', 'Profile','Software Version','Factory Software Version', 'Build Number', 'Model','Serial Number','Created', 'Activated', 'Last Contact'])
                            print(lenrow)
                            for p in listrow[0:lenrow]:
                                ros.append(p)
                #print(ros)

                filenamesdwan = 'sdwan_'+str(date_time)+'.csv'
                with open(filenamesdwan,'a') as csvs:
                    files = csv.writer(csvs)
                    files.writerows(ros)

                # sdwanfile = pd.read_csv(filenamesdwan)
                # sdwandataframe = pd.DataFrame(sdwanfile)


                ##connect database and send data into it###
                cursor = db.cursor()
                with open(filenamesdwan, 'r') as read:
                    incsv = csv.reader(read)
                    csv_data = list(incsv)
                    lenrow = len(csv_data)
                    apCountList = []
                    for col in csv_data[1:lenrow]:
                        #print(col[:10])
                        
                        edge = col[0]
                        #print(edge)
                        status = col[1]
                        ha = col[2]
                        connectedlink = col[3]
                        degradedlink = col[4]
                        disconnectedlink = col[5]
                        otherlinkstates = col[6]
                        cloudservicestatus = col[7]
                        profile = col[8]
                        softwareversion = col[9]
                        factorysoftwareversion = col[10]
                        buildno = col[11]
                        model = col[12]
                        sn = col[13]
                        createdDate = col[14]
                        activated = col[15]
                        lastcontace = col[16]


                    #     apCountList.append(PrimeCtrl)
                    #     #apCountList.append()
                        cursor = db.cursor()
                        sqlsdwan = """INSERT INTO sdwan_alldata 
                                    (edges, status, ha, connectedLinks, degradeLinks, disconnectLinks, otherLinksStatus,
                                    cloudServiceStatus, profile, softwareVersion, factorySoftwareVersion, buildNo,
                                    model, sn, createdDate, activate, lastContact, created, updated)
                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """
                        values = (edge, status, ha, connectedlink, degradedlink, disconnectedlink, otherlinkstates, cloudservicestatus, profile, softwareversion, factorysoftwareversion, buildno, model, sn, createdDate, activated, lastcontace, daytime, daytime)
                        
                        cursor.execute(sqlsdwan, values)
                        db.commit()
                        #cursor.close()
                    
                    #cursor.close()
                    print('process: ทำงานเสร็จสิ้น...')

                

            
                




            except IOError as e:
                print(e)
                pass
        

def processalerts():
    try:
        pathsRead = '/home/systemx/BitGitDJANGO/django_restapi/checklist'
        listdata = []
        listof = os.listdir(pathsRead)
        pattern = 'alert*.csv'
        count = 0
        for e in listof:
            count = count+1
                    
            if fnmatch.fnmatch(e, pattern):
                print(e)
                with open(e, 'r') as f:
                    csv_read = csv.reader(f)
                    listrow = list(csv_read)
                    lenrow = len(listrow)
                    #ros.append(['Edge',	'Status', 'HA',	'Connected Links',	'Degraded Links', 'Disconnected Links',	'Other Link States','Cloud Services Status', 'Profile','Software Version','Factory Software Version', 'Build Number', 'Model','Serial Number','Created', 'Activated', 'Last Contact'])
                    print(lenrow)
                    for p in listrow[0:lenrow]:
                        listdata.append(p)
                #print(ros)
        filenamesdwanalertdata = 'sdwan_alert_'+str(date_time)+'.csv'
        with open(filenamesdwanalertdata,'a') as csvs:
            files = csv.writer(csvs)
            files.writerows(listdata)

        cursor = db.cursor()
        with open(filenamesdwanalertdata, 'r') as read:
            incsv = csv.reader(read)
            csv_data = list(incsv)
            lenrow = len(csv_data)
            apCountList = []
            for col in csv_data[1:lenrow]:
                notice = col[0]
                category = col[1]
                typestatus = col[2]
                description = col[3]
                status = col[4]
                sender = col[5]
                recipients = col[6]


                cursor = db.cursor()
                sqlsdwan = """INSERT INTO sdwan_dataalert 
                              (noticeTime, category, typestatus, 
                               decription, status, sender, recipients, 
                               created, updated)
                               VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) """
                values = (notice, category, typestatus, description, status, sender, recipients, daytime, daytime)
                        
                cursor.execute(sqlsdwan, values)
                db.commit()
        print('process: ทำงานเสร็จสิ้น...')
        ###delete all .csv files after processing and send to database###
        filelist = [f for f in os.listdir(pathsRead) if f.endswith('.csv') ]
        for f in filelist:
            os.remove(os.path.join(pathsRead, f))
        ###delete all .csv files after processing and send to database###
    except IOError as identifier:
        print(identifier)
        pass