from django.db import models

class edges(models.Model):
    down = models.CharField(max_length=200, default="")
    degraded = models.CharField(max_length=200, default="")
    connected = models.CharField(max_length=200, default="")
    outOfActivated = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class links(models.Model):
    down = models.CharField(max_length=200, default="")
    unstable = models.CharField(max_length=200, default="")
    stable = models.CharField(max_length=200, default="")
    alllinks = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class alldata(models.Model):
    edges  = models.CharField(max_length=200, default="")
    status  = models.CharField(max_length=200, default="")
    ha  = models.CharField(max_length=200, default="")
    connectedLinks  = models.CharField(max_length=200, default="")
    degradeLinks  = models.CharField(max_length=200, default="")
    disconnectLinks  = models.CharField(max_length=200, default="")
    otherLinksStatus  = models.CharField(max_length=200, default="")
    cloudServiceStatus  = models.CharField(max_length=200, default="")
    profile  = models.CharField(max_length=200, default="")
    softwareVersion  = models.CharField(max_length=200, default="")
    factorySoftwareVersion  = models.CharField(max_length=200, default="")
    buildNo  = models.CharField(max_length=200, default="")
    model  = models.CharField(max_length=200, default="")
    sn  = models.CharField(max_length=200, default="")
    createdDate  = models.CharField(max_length=200, default="")
    activate  = models.CharField(max_length=200, default="")
    lastContact  = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class dataalert(models.Model):
    noticeTime = models.CharField(max_length=200, default="")
    category = models.CharField(max_length=200, default="")
    typestatus = models.CharField(max_length=200, default="")
    decription = models.CharField(max_length=200, default="")
    status = models.CharField(max_length=200, default="")
    sender = models.CharField(max_length=200, default="")
    recipients = models.CharField(max_length=500, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)