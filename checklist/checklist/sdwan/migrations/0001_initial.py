# Generated by Django 2.2.6 on 2019-12-25 08:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='edges',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('down', models.CharField(default='', max_length=200)),
                ('degraded', models.CharField(default='', max_length=200)),
                ('connected', models.CharField(default='', max_length=200)),
                ('outOfActivated', models.CharField(default='', max_length=200)),
                ('created', models.DateField(auto_now_add=True, null=True)),
                ('updated', models.DateField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='links',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('down', models.CharField(default='', max_length=200)),
                ('unstable', models.CharField(default='', max_length=200)),
                ('stable', models.CharField(default='', max_length=200)),
                ('alllinks', models.CharField(default='', max_length=200)),
                ('created', models.DateField(auto_now_add=True, null=True)),
                ('updated', models.DateField(auto_now=True, null=True)),
            ],
        ),
    ]
