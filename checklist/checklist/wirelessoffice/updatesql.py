import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host='localhost',
                                        database='checklistdata',
                                        user='root',
                                        password='xcalculusx')
    cursor =  connection.cursor()
    sql = 'select * from wirelessoffice_wireofficedown'
    cursor.execute(sql)
    record = cursor.fetchall()
    # print(record)
    for r in record:
        if r[5] == 'Down' and r[6] == 'Down':
            #print(r[0])
            sql_update = '''
                update wirelessoffice_wireofficedown set Status802bgn = 'UP' , Status802ac = 'Up' where id = '%s' 
            '''
            values = r[0]
            cursor.execute(sql_update, (values,))
            connection.commit()
        elif r[5] == 'Down':
            sql_update = '''
                update wirelessoffice_wireofficedown set Status802bgn = 'UP'  where id = '%s' 
            '''
            values = r[0]
            cursor.execute(sql_update, (values,))
            connection.commit()
        
    #print(record)

except mysql.connector.Error as e:
    print("Failed to update table record: {}".format(e))

finally:
    if (connection.is_connected()):
        connection.close()
        print("MySQL connection is closed")