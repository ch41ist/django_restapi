import email
import getpass, imaplib
import os
import sys
from zipfile import ZipFile
import time

import glob
import csv
from itertools import islice
import pandas as pd
from datetime import datetime, timedelta, date
import fnmatch
now = datetime.now()
# import timedelta
date_time = now.strftime("%m_%d_%Y")


daytime = datetime.today()


date = (datetime.today() - timedelta(1)).strftime("%d-%b-%Y")

# datetime.today
import psutil
import mysql.connector

db = mysql.connector.connect(
    host = 'localhost',
    user = 'root',
    password = 'xcalculusx',
    database = 'checklistdata'
)





def downloadmail():
    detach_dir = '.'
    if 'attachments' not in os.listdir(detach_dir):
        os.mkdir('attachments')

    userName = 'imap10x@gmail.com'
    passwd = 'xcalculusx'

    try:
        imapSession = imaplib.IMAP4_SSL('imap.gmail.com')
        typ, accountDetails = imapSession.login(userName, passwd)
        print(typ)
        if typ != 'OK':
            print ('Not able to sign in!')
            raise
        
        imapSession.select('INBOX')
        typ, data = imapSession.search(None, '(SENTSINCE {0})'.format(date))
        print(typ)
        if typ != 'OK':
            print ('Error searching Inbox.')
            raise
        
        # Iterating over all emails
        for msgId in data[0].split():
            typ, messageParts = imapSession.fetch(msgId, '(RFC822)')
            print(typ)
            #print(msgId)
            # if typ != 'OK':
            #     print ('Error fetching mail.')
            #     raise

            emailBody = messageParts[0][1]
            #print(emailBody)
            mail = email.message_from_bytes(emailBody)
            #print(mail)
            for part in mail.walk():
                if part.get_content_maintype() == 'multipart':
                    # print part.as_string()
                    continue
                if part.get('Content-Disposition') is None:
                    # print part.as_string()
                    continue
                fileName = part.get_filename()

                if bool(fileName):
                    filePath = os.path.join(detach_dir, 'attachments', fileName)
                    if not os.path.isfile(filePath) :
                        print (fileName)
                        fp = open(filePath, 'wb')
                        fp.write(part.get_payload(decode=True))
                        fp.close()

                
        imapSession.close()
        imapSession.logout()

    except :
        print ('Not able to download all attachments.')


def un7z():
    part = '/home/systemx/BitGitDJANGO/django_restapi/checklist/checklist/wirelessoffice/attachments'
    files = '.zip'
    os.chdir(part)
    for item in os.listdir(part):
        if item.endswith(files):
            file_name = os.path.abspath(item)
            zip_f = ZipFile(file_name)
            zip_f.extractall(part)
            zip_f.close()
            os.remove(file_name)
    print('แตกไฟล์เสร็จละจ้าา')


def processfiles():
    pathsRead = '/home/systemx/BitGitDJANGO/django_restapi/checklist/checklist/wirelessoffice/attachments'
    ros = []
    listof = os.listdir(pathsRead)
    pattern = 'AP_Sum_*.csv'
    count = 0
    for e in listof:
        count = count+1
        
        if fnmatch.fnmatch(e, pattern):
            print(e)
            with open(e, 'r') as f:
                csv_read = csv.reader(f)
                listrow = list(csv_read)
                lenrow = len(listrow)
                ros.append(['AP Name', 'AP IP Address', 'Model', 'AP Mode', '802.11b/g/n Status', '802.11a/n/ac Status', 'Ethernet MAC', 'Primary Controller Name', 'Secondary Controller Name', 'Serial Number'])
                print(lenrow)
                for p in listrow[19:lenrow]:
                    ros.append(p)

    filename = 'prefix_'+str(date_time)+'.csv'
    with open(filename,'a') as csvs:
        files = csv.writer(csvs)
        files.writerows(ros)

    l = pd.read_csv(filename)
    f = pd.DataFrame(l)
    status802ac = f.loc[f['802.11b/g/n Status'] == 'Down'] 

    filenames = 'processed_'+str(date_time)+'.csv'
    status802ac.to_csv(filenames)


def sendtodatabases():
    cursor = db.cursor()
    filenames = 'processed_'+str(date_time)+'.csv'
    with open(filenames, 'r') as read:
        incsv = csv.reader(read)
        csv_data = list(incsv)
        lenrow = len(csv_data)
        for row in csv_data[1:lenrow]:

            APName = row[1]
            APIP = row[2]
            APModel = row[3]
            APMode = row[4]
            AP802bgn = row[5]
            AP802ac = row[6]
            APMAC = row[7]
            PrimeCtrl = row[8]
            SecCtrl = row[9]
            SN = row[10]

            #print(APName)


            sqlmb = """INSERT INTO wirelessoffice_wireofficedown 
                        (APname, APIP, APModel, APMode, Status802bgn,
                        Status802ac, EtherMAC, PrimeCtrlName, SecCtrlName, SN, created, updated)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """
            values = (APName, APIP, APModel, APMode, AP802bgn, AP802ac, APMAC, PrimeCtrl, SecCtrl, SN, daytime, daytime)
            
            cursor.execute(sqlmb, values)
            db.commit()
            #cursor.close()
        print('process: F5 ทำงานเสร็จสิ้น...')
            
            # print(row[5])

def removeallFile():
    dirs = '/home/systemx/BitGitDJANGO/django_restapi/checklist/checklist/wirelessoffice/attachments'
    filelist = [f for f in os.listdir(dirs) if f.endswith('.csv') ]
    for f in filelist:
        os.remove(os.path.join(dirs, f))
        


downloadmail()
time.sleep(5)
un7z()
time.sleep(5)
processfiles()
time.sleep(10)
sendtodatabases()
time.sleep(20)
removeallFile()
process = psutil.Process(os.getpid())

byte = process.memory_info().rss
mb = byte/1000000
convert = str(mb)
print('ใช้แรมระหว่างการทำงานทั้งหมด: '+convert+' Mb')

