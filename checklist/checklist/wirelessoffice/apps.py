from django.apps import AppConfig


class WirelessofficeConfig(AppConfig):
    name = 'wirelessoffice'
