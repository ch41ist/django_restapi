from django.shortcuts import render
from checklist.wirelessoffice.models import wireofficeDown
import mysql.connector
from datetime import date
from django.conf import settings
from django.core.files.storage import FileSystemStorage

import email
import getpass, imaplib
import os
import sys
from zipfile import ZipFile
import time

import glob
import csv
from itertools import islice
import pandas as pd
from datetime import datetime, timedelta, date
import fnmatch
import psutil
import mysql.connector
import pandas as pd
from django.core.files import File

#from checklist.wirelessoffice.processdata import processfiles

now = datetime.now()
date_time = now.strftime("%m_%d_%Y")
daytime = datetime.today()

db = mysql.connector.connect(
    host = 'localhost',
    user = 'root',
    password = 'xcalculusx',
    database = 'checklistdata'
)

def wireless(request):
    wirelessM = wireofficeDown.objects.filter(created=date.today())
    context = {
        'wireless': wirelessM,
    }

    if request.method == 'POST':
        myfile = request.FILES['myfile']
        fs = FileSystemStorage(location='django_restapi/checklist/attachments')
        fs.save(myfile.name, myfile)


        import zipfile
        zips = zipfile.ZipFile(myfile, 'r')
        zips.extractall()

        filelist = [f for f in os.listdir('attachments') if f.endswith('.zip') ]
        for f in filelist:
            os.remove(os.path.join('attachments', f))
        zips.close()

        print('แตกไฟล์เสร็จละจ้าา')
        time.sleep(5)

        processfiles()

        

    return render(request,'wireless.html', context)


def processfiles():
    
            filename  = os.path.expanduser('/home/systemx/BitGitDJANGO/django_restapi/checklist/attachments/*.csv') 
            try:
                ###processing files after unzip and make new concate files that collect from source file###
                pathsRead = '/home/systemx/BitGitDJANGO/django_restapi/checklist'
                ros = []
                listof = os.listdir(pathsRead)
                pattern = 'AP_Sum_*.csv'
                count = 0
                for e in listof:
                    count = count+1
                    
                    if fnmatch.fnmatch(e, pattern):
                        print(e)
                        with open(e, 'r') as f:
                            csv_read = csv.reader(f)
                            listrow = list(csv_read)
                            lenrow = len(listrow)
                            ros.append(['AP Name', 'AP IP Address', 'Model', 'AP Mode', '802.11b/g/n Status', '802.11a/n/ac Status', 'Ethernet MAC', 'Primary Controller Name', 'Secondary Controller Name', 'Serial Number'])
                            print(lenrow)
                            for p in listrow[19:lenrow]:
                                ros.append(p)

                filename = 'prefix_'+str(date_time)+'.csv'
                with open(filename,'a') as csvs:
                    files = csv.writer(csvs)
                    files.writerows(ros)

                l = pd.read_csv(filename)
                f = pd.DataFrame(l)
                status802ac = f.loc[f['802.11b/g/n Status'] == 'Down'] 

                filenames = 'processed_'+str(date_time)+'.csv'
                status802ac.to_csv(filenames)
                ###processing files after unzip and make new concate files that collect from source file###

                ###connect database and send data into it###
                cursor = db.cursor()
                with open(filenames, 'r') as read:
                    incsv = csv.reader(read)
                    csv_data = list(incsv)
                    lenrow = len(csv_data)
                    apCountList = []
                    for col in csv_data[1:lenrow]:
                        
                        APName = col[1]
                        APIP = col[2]
                        APModel = col[3]
                        APMode = col[4]
                        AP802bgn = col[5]
                        AP802ac = col[6]
                        APMAC = col[7]
                        PrimeCtrl = col[8]
                        SecCtrl = col[9]
                        SN = col[10]

                        apCountList.append(PrimeCtrl)
                        #apCountList.append()

                        sqlmb = """INSERT INTO wirelessoffice_wireofficedown 
                                    (APname, APIP, APModel, APMode, Status802bgn,
                                    Status802ac, EtherMAC, PrimeCtrlName, SecCtrlName, SN, created, updated)
                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """
                        values = (APName, APIP, APModel, APMode, AP802bgn, AP802ac, APMAC, PrimeCtrl, SecCtrl, SN, daytime, daytime)
                        
                        cursor.execute(sqlmb, values)
                        db.commit()
                        #
                        
                    for team in [ele for ind, ele in enumerate(apCountList,1) if ele not in apCountList[ind:]]:
                        count = 0
                        for ele in apCountList:
                            if team == ele:
                                count += 1
                        print("{} {}".format(team,count))
                        sqlwlc = """
                            insert into wirelessoffice_countap
                            (wlcName, wlcCount, created, updated)
                            values ( %s, %s, %s, %s)
                        """
                        wlcvalues = (team, count, daytime, daytime)
                        cursor.execute(sqlwlc, wlcvalues)
                        db.commit()
                        count = 0
                    print(len(team))
                    cursor.close()
                    print('process: ทำงานเสร็จสิ้น...')
                ###connect database and send data into it###
                
                ######
                
                ######

                ###delete all .csv files after processing and send to database###
                filelist = [f for f in os.listdir(pathsRead) if f.endswith('.csv') ]
                for f in filelist:
                    os.remove(os.path.join(pathsRead, f))
                ###delete all .csv files after processing and send to database###


            except IOError as e:
                print(e)
        




