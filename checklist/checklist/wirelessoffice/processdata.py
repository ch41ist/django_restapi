import email
import getpass, imaplib
import os
import sys
from zipfile import ZipFile
import time

import glob
import csv
from itertools import islice
import pandas as pd
from datetime import datetime, timedelta, date
import fnmatch
now = datetime.now()
# import timedelta
date_time = now.strftime("%m_%d_%Y")


daytime = datetime.today()


date = (datetime.today() - timedelta(1)).strftime("%d-%b-%Y")

import psutil
import mysql.connector

db = mysql.connector.connect(
    host = 'localhost',
    user = 'root',
    password = 'xcalculusx',
    database = 'checklistdata'
)


def processfiles():
    pathsRead = '/home/systemx/BitGitDJANGO/django_restapi/checklist/attachments'
    ros = []
    listof = os.listdir(pathsRead)
    pattern = 'AP_Sum_*.csv'
    count = 0
    for e in listof:
        count = count+1
        
        if fnmatch.fnmatch(e, pattern):
            print(e)
            with open(e, 'r') as f:
                csv_read = csv.reader(f)
                listrow = list(csv_read)
                lenrow = len(listrow)
                ros.append(['AP Name', 'AP IP Address', 'Model', 'AP Mode', '802.11b/g/n Status', '802.11a/n/ac Status', 'Ethernet MAC', 'Primary Controller Name', 'Secondary Controller Name', 'Serial Number'])
                print(lenrow)
                for p in listrow[19:lenrow]:
                    ros.append(p)

    filename = 'prefix_'+str(date_time)+'.csv'
    with open(filename,'a') as csvs:
        files = csv.writer(csvs)
        files.writerows(ros)

    l = pd.read_csv(filename)
    f = pd.DataFrame(l)
    status802ac = f.loc[f['802.11b/g/n Status'] == 'Down'] 

    filenames = 'processed_'+str(date_time)+'.csv'
    status802ac.to_csv(filenames)

processfiles()


