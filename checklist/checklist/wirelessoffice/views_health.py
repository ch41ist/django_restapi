from django.shortcuts import render
from checklist.wirelessoffice.models import wireofficeDown, controllerhealthCPU, controllerhealthRAM
import mysql.connector
from datetime import date
from django.conf import settings
from django.core.files.storage import FileSystemStorage

import email
import getpass, imaplib
import os
import sys
from zipfile import ZipFile
import time

import glob
import csv
from itertools import islice
import pandas as pd
from datetime import datetime, timedelta, date
import fnmatch
import psutil
import mysql.connector
import pandas as pd
from django.core.files import File

now = datetime.now()
date_time = now.strftime("%m_%d_%Y")
daytime = datetime.today()

db = mysql.connector.connect(
    host = 'localhost',
    user = 'root',
    password = 'xcalculusx',
    database = 'checklistdata'
)

def health(request):
    healthcpu = controllerhealthCPU.objects.filter().order_by('-id')[:5]
    healthram = controllerhealthRAM.objects.filter().order_by('-id')[:5]
    #print(health)
    context = {
        'healthcpu': healthcpu,
        'healthram': healthram,
    }
    if request.method == 'POST':
        myfile = request.FILES['myfile']
        fs = FileSystemStorage(location='django_restapi/checklist/attachments')
        fs.save(myfile.name, myfile)


        import zipfile
        zips = zipfile.ZipFile(myfile, 'r')
        zips.extractall()

        filelist = [f for f in os.listdir('attachments') if f.endswith('.zip') ]
        for f in filelist:
            os.remove(os.path.join('attachments', f))
        zips.close()

        print('แตกไฟล์เสร็จละจ้าา')
        time.sleep(5)
        processfiles()

    return render(request,'controller.html', context)


def processfiles():
    
            #filename  = os.path.expanduser('/home/systemx/BitGitDJANGO/django_restapi/checklist/attachments/*.csv') 
            try:
                ###processing files after unzip and make new concate files that collect from source file###
                pathsRead = '/home/systemx/BitGitDJANGO/django_restapi/checklist'
                ros = []
                roi = []
                listof = os.listdir(pathsRead)
                pattern = 'test_health_*.csv'
                count = 0
                for e in listof:
                    count = count+1
                    
                    if fnmatch.fnmatch(e, pattern):
                        print(e)
                        with open(e, 'r') as f:
                            csv_read = csv.reader(f)
                            listrow = list(csv_read)
                            lenrow = len(listrow)
                            ros.append(['device ip address', 'device name', 'Cpu Instance', 'cpu min(%)', 'cpu avg(%)', 'cpu max(%)'])
                            for p in listrow[9:14]:
                                ros.append(p)
                            roi.append(['device ip address', 'device name', 'memory pool', 'min(%)', 'avg(%)', 'max(%)'])
                            for s in listrow[18:22]:
                                roi.append(s)

                filenamecpu = 'prefix_cpu_'+str(date_time)+'.csv'
                with open(filenamecpu,'a') as csvs:
                    files = csv.writer(csvs)
                    files.writerows(ros)
                
                filenameram = 'prefix_ram_'+str(date_time)+'.csv'
                with open(filenameram,'a') as csvs:
                    files = csv.writer(csvs)
                    files.writerows(roi)
                
                
                ###processing files after unzip and make new concate files that collect from source file###

                ###connect database and send data into it###
                cursor = db.cursor()
                with open(filenamecpu, 'r') as read:
                    incsv = csv.reader(read)
                    csv_data = list(incsv)
                    lenrow = len(csv_data)
                    apCountList = []
                    for col in csv_data[1:lenrow]:
                        
                        deviceIP = col[0]
                        deviceName = col[1]
                        deviceCPUInstance = col[2]
                        deviceCPUMin = col[3]
                        deviceCPUMax = col[4]
                        deviceCPUAvg = col[5]
                        
                        sqlwlccpu = """
                            insert into wirelessoffice_controllerhealthcpu
                            (deviceIP, deviceName, cpuInstance, deviceCPUmin, deviceCPUmax, deviceCPUavg, created, updated)
                            values (%s, %s, %s, %s, %s, %s, %s, %s)
                        """
                        valuescpu = (deviceIP, deviceName, deviceCPUInstance, deviceCPUMin, deviceCPUMax, deviceCPUAvg, daytime, daytime)

                        cursor.execute(sqlwlccpu, valuescpu)
                        db.commit()

                    print('process: ทำงานเสร็จสิ้น...')
                

                with open(filenameram, 'r') as readRAM:
                    intcsv = csv.reader(readRAM)
                    csv_dataRAM = list(intcsv)
                    lenrows = len(csv_dataRAM)
                    apCountList = []
                    for colRAM in csv_dataRAM[1:lenrows]:

                        #print(colRAM)

                        deviceIP = colRAM[0]
                        deviceName = colRAM[1]
                        memoryPool = colRAM[2]
                        deviceRAMMin = colRAM[3]
                        deviceRAMMax = colRAM[4]
                        deviceRAMAvg = colRAM[5]
                        
                        sqlwlcram = """
                            insert into wirelessoffice_controllerhealthram
                            (deviceIP, deviceName, memorypool, deviceRAMmin, deviceRAMmax, deviceRAMavg, created, updated)
                            values (%s, %s, %s, %s, %s, %s, %s, %s)
                        """
                        valuesram = (deviceIP, deviceName, memoryPool, deviceRAMMin, deviceRAMMax, deviceRAMAvg, daytime, daytime)

                        cursor.execute(sqlwlcram, valuesram)
                        db.commit()

                filelist = [f for f in os.listdir(pathsRead) if f.endswith('.csv') ]
                for f in filelist:
                    os.remove(os.path.join(pathsRead, f))
                ###connect database and send data into it###


            except IOError as e:
                print(e)