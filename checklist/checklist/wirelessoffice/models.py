from django.db import models



class wireofficeDown(models.Model):
    APname = models.CharField(max_length=200, default="")
    APIP = models.CharField(max_length=1000, default="")
    APModel = models.CharField(max_length=1000, default="")
    APMode = models.CharField(max_length=1000, default="")
    Status802bgn = models.CharField(max_length=200, default="")
    Status802ac = models.CharField(max_length=200, default="")
    EtherMAC = models.CharField(max_length=1000, default="")
    PrimeCtrlName = models.CharField(max_length=200, default="")
    SecCtrlName = models.CharField(max_length=200, default="")
    SN = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class controllerhealthCPU(models.Model):
    deviceName = models.CharField(max_length=200, default="")
    deviceIP = models.CharField(max_length=200, default="")
    cpuInstance = models.CharField(max_length=200, default="")
    deviceCPUmin = models.CharField(max_length=200, default="")
    deviceCPUmax = models.CharField(max_length=200, default="")
    deviceCPUavg = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class controllerhealthRAM(models.Model):
    deviceName = models.CharField(max_length=200, default="")
    deviceIP = models.CharField(max_length=200, default="")
    memorypool = models.CharField(max_length=200, default="")
    deviceRAMmin = models.CharField(max_length=200, default="")
    deviceRAMmax = models.CharField(max_length=200, default="")
    deviceRAMavg = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class countap(models.Model):
    wlcName = models.CharField(max_length=200, default="")
    wlcCount = models.CharField(max_length=200, default="")
    #allwlc = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)
