# Generated by Django 2.2.4 on 2019-09-30 08:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voice', '0008_remove_voicedescription_workname'),
    ]

    operations = [
        migrations.AlterField(
            model_name='voicedescription',
            name='status',
            field=models.CharField(default='', max_length=200),
        ),
    ]
