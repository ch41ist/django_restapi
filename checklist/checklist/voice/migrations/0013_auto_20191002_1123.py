# Generated by Django 2.2.4 on 2019-10-02 04:23

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voice', '0012_voicedataautoscripts_uptime'),
    ]

    operations = [
        migrations.AlterField(
            model_name='voicedataautoscripts',
            name='created',
            field=models.DateTimeField(blank=True, null=True, verbose_name=datetime.datetime(2019, 10, 2, 11, 23, 16, 233896)),
        ),
        migrations.AlterField(
            model_name='voicedataautoscripts',
            name='updated',
            field=models.DateTimeField(blank=True, null=True, verbose_name=datetime.datetime(2019, 10, 2, 11, 23, 16, 233946)),
        ),
    ]
