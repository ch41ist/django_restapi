# Generated by Django 2.2.4 on 2019-09-25 07:41

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voice', '0005_auto_20190925_0739'),
    ]

    operations = [
        migrations.AlterField(
            model_name='voicedescription',
            name='created',
            field=models.DateTimeField(blank=None, default=datetime.datetime(2019, 9, 25, 7, 41, 30, 171502), null=None),
        ),
        migrations.AlterField(
            model_name='voicedescription',
            name='updated',
            field=models.DateTimeField(blank=None, default=datetime.datetime(2019, 9, 25, 7, 41, 30, 171561), null=None),
        ),
    ]
