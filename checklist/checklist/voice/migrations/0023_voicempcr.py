# Generated by Django 2.2.6 on 2020-01-23 08:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voice', '0022_auto_20200108_1619'),
    ]

    operations = [
        migrations.CreateModel(
            name='voiceMPCR',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hostname', models.CharField(default='', max_length=200)),
                ('IP', models.CharField(default='', max_length=200)),
                ('checklister', models.CharField(default='', max_length=200)),
                ('dateStatus', models.CharField(default='', max_length=200)),
                ('status', models.CharField(default='', max_length=200)),
                ('description', models.CharField(default='', max_length=1000)),
                ('created', models.DateField(auto_now_add=True, null=True)),
                ('updated', models.DateField(auto_now=True, null=True)),
            ],
        ),
    ]
