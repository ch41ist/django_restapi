# Generated by Django 2.2.4 on 2019-10-01 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voice', '0011_auto_20191001_0955'),
    ]

    operations = [
        migrations.AddField(
            model_name='voicedataautoscripts',
            name='uptime',
            field=models.CharField(default='', max_length=200),
        ),
    ]
