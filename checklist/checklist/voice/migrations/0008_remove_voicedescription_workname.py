# Generated by Django 2.2.4 on 2019-09-30 08:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('voice', '0007_auto_20190925_0743'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='voicedescription',
            name='workname',
        ),
    ]
