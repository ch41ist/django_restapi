from django.shortcuts import render
import mysql.connector
from datetime import date
from checklist.voice.models import voicediskcapacitycript , voicedataautoscriptsHttpd
from checklist.voice.models import voicedataautoscripts


def dataTable(request):
    voice_disk_cap = voicediskcapacitycript.objects.all()
    voice_asterisk = voicedataautoscripts.objects.all()
    voice_httpd = voicedataautoscriptsHttpd.objects.all()
    #print(voice_disk_cap)
    context = {'voice_disk_cap': voice_disk_cap,
               'voice_asterisk': voice_asterisk,
               'voice_httpd': voice_httpd
              }
    return render(request,'voicedataTable.html', context)