from rest_framework import serializers
from checklist.voice.models import  voicedataautoscripts

class voicedescriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = voicedescription
        fields = ['id', 'descriptions', 'ip', 'howto']
