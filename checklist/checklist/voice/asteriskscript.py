from django.shortcuts import render
import netmiko
import json
from datetime import datetime, timedelta
import time, threading
import schedule
import time

#
from checklist.voice.models import voicedataautoscripts, voicedataautoscriptsHttpd, voicediskcapacitycript

times = 40

def voicedataasterisk():
    devices = """
        110.170.246.201
        110.170.246.202
        110.170.246.203
        """.strip().splitlines()

    device_type = 'cisco_ios'
    username = 'root'
    password = 'CbcGosoftippbx9779'

    netmiko_exceptions = (netmiko.ssh_exception.NetMikoTimeoutException,
                          netmiko.ssh_exception.NetMikoAuthenticationException)

    for device in devices:
        # datastore = []
        # alldata = []
        try:
            print('~' * 79)
            print('Connecting to device:', device)
            connection = netmiko.ConnectHandler(ip=device, device_type=device_type,
                                                username=username, password=password)
            sendcommandasterisk = connection.send_command("service asterisk status | grep Active: ")
            # datastore = sendcommandasterisk.split(" ")
            print(sendcommandasterisk)
            # outputx = sendcommandasterisk
            asteriskactive = sendcommandasterisk.find(':') + 8
            asteriskrunning = sendcommandasterisk.find('ng)') + 3
            asteriskstartTime = sendcommandasterisk.find(';') + 72
            asteriskendTime = sendcommandasterisk.find('ago') + 3
            asteriskstatus = sendcommandasterisk[asteriskactive:asteriskrunning]
            asteriskallTemp = sendcommandasterisk[asteriskactive:asteriskendTime]
            asteriskoutTime = sendcommandasterisk[asteriskstartTime:asteriskendTime]

            sendcommandahttpd = connection.send_command("service httpd status | grep Active: ")
            # datastored = sendcommandahttpd.split(" ")
            httpdactive = sendcommandahttpd.find(':') + 8
            httpdrunning = sendcommandahttpd.find('ng)') + 3
            httpdstartTime = sendcommandahttpd.find(';') + 72
            httpdendTime = sendcommandahttpd.find('ago') + 3
            httpdstatus = sendcommandahttpd[httpdactive:httpdrunning]
            httpdallTemp = sendcommandahttpd[httpdactive:httpdendTime]
            httpdoutTime = sendcommandahttpd[httpdstartTime:httpdendTime]
            print(sendcommandahttpd)

            if (asteriskstatus == 'active (running)' and httpdstatus == 'active (running)'):
                saveDatabaseAsterisk = voicedataautoscripts(
                    ip=device,
                    workname='service asterisk status',
                    descriptions=asteriskallTemp,
                    howto='running Script by web browser',
                    status=asteriskstatus,
                    uptime=asteriskoutTime
                )
                saveDatabaseHttpd = voicedataautoscriptsHttpd(
                    ip=device,
                    workname='service httpd status',
                    descriptions=httpdallTemp,
                    howto='running Script by web browser',
                    status=httpdstatus,
                    uptime=httpdoutTime
                )
                saveDatabaseAsterisk.save()
                saveDatabaseHttpd.save()
                connection.disconnect()
            else:
                fixedcommand = connection.send_command("service asterisk start && service httpd start")

                time.sleep(40)

                sendcommandasteriskafterdown = connection.send_command("service asterisk status | grep Active: ")
                # datastore = sendcommandasterisk.split(" ")
                print(sendcommandasteriskafterdown)
                # outputx = sendcommandasterisk
                asteriskactiveafterdown = sendcommandasteriskafterdown.find(':') + 8
                asteriskrunningafterdown = sendcommandasteriskafterdown.find('ng)') + 3
                asteriskstartTimeafterdown = sendcommandasteriskafterdown.find(';') + 72
                asteriskendTimeafterdown = sendcommandasteriskafterdown.find('ago') + 3
                asteriskstatusafterdown = sendcommandasteriskafterdown[asteriskactiveafterdown:asteriskrunningafterdown]
                asteriskallTempafterdown = sendcommandasteriskafterdown[
                                           asteriskactiveafterdown:asteriskendTimeafterdown]
                asteriskoutTimeafterdown = sendcommandasteriskafterdown[
                                           asteriskstartTimeafterdown:asteriskendTimeafterdown]

                sendcommandahttpdafterdown = connection.send_command("service httpd status | grep Active: ")
                # datastored = sendcommandahttpd.split(" ")
                httpdactiveafterdown = sendcommandahttpdafterdown.find(':') + 8
                httpdrunningafterdown = sendcommandahttpdafterdown.find('ng)') + 3
                httpdstartTimeafterdown = sendcommandahttpdafterdown.find(';') + 72
                httpdendTimeafterdown = sendcommandahttpdafterdown.find('ago') + 3
                httpdstatusafterdown = sendcommandahttpdafterdown[httpdactiveafterdown:httpdrunningafterdown]
                httpdallTempafterdown = sendcommandahttpdafterdown[httpdactiveafterdown:httpdendTimeafterdown]
                httpdoutTimeafterdown = sendcommandahttpdafterdown[httpdstartTimeafterdown:httpdendTimeafterdown]
                print(sendcommandahttpdafterdown)

                saveDatabaseAsterisk = voicedataautoscripts(
                    ip=device,
                    workname='service asterisk status',
                    descriptions=asteriskallTempafterdown,
                    howto='running Script by web browser',
                    status=asteriskstatusafterdown,
                    uptime=asteriskoutTimeafterdown
                )
                saveDatabaseHttpd = voicedataautoscriptsHttpd(
                    ip=device,
                    workname='service httpd status',
                    descriptions=httpdallTempafterdown,
                    howto='running Script by web browser',
                    status=httpdstatusafterdown,
                    uptime=httpdoutTimeafterdown
                )
                saveDatabaseAsterisk.save()
                saveDatabaseHttpd.save()
                print(fixedcommand)
                connection.disconnect()

        except netmiko_exceptions as e:
            print('Failed to ', device, e)
            saveDatabaseAsterisk = voicedataautoscripts(
                ip=device,
                workname='service asterisk status',
                descriptions='unable to connect host',
                howto='running Script by web browser',
                status='unable to connect host',
                uptime='unable to connect host'
            )
            saveDatabaseHttpd = voicedataautoscriptsHttpd(
                ip=device,
                workname='service httpd status',
                descriptions='unable to connect host',
                howto='running Script by web browser',
                status='unable to connect host',
                uptime='unable to connect host'
            )
            saveDatabaseAsterisk.save()
            saveDatabaseHttpd.save()
            connection.disconnect()

    threading.Timer(times, voicedataasterisk).start()


    


#voicedataasterisk()



