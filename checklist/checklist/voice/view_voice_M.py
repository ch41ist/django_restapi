from django.shortcuts import render
import mysql.connector
import datetime
daytime = datetime.date.today()
from checklist.voice.models import voicediskcapacitycript , voicedataautoscriptsHttpd
from checklist.voice.models import voicedataautoscripts, voiceMPCR, voiceMCTIR, voiceMTSCC, voiceMCSCUCM, voiceMCRTM, voiceMSTA
from django.utils.datastructures import MultiValueDictKeyError


def manual(request):
    
    entrydata = 'testdata'
    
    ip0 = '10.151.36.61'
    ip1 = '10.151.36.23'
    ip2 = '10.182.236.72'
    ip3 = '10.182.247.78'

    hostname0 = 'tarcucmpub'
    hostname1 = 'tarcucm01'
    hostname2 = 'sbrcucm02'
    hostname3 = 'tarcucmjb01'

    hostname6 = 'gomeeting.cpall.co.th'
    ip6 = '10.151.19.80'
    hostname7 = 'gomeeting2.cpall.co.th'
    ip7 = '10.151.19.34'

    
    if request.method == 'POST':

        if request.POST:
            print("เจอหว่ะ")
            try:
                
                
                status1A = [request.POST["status1A"], hostname0, ip0]
                status1B = [request.POST["status1B"], hostname1, ip1]
                status1C = [request.POST["status1C"], hostname2, ip2]
                status1D = [request.POST["status1D"], hostname3, ip3]
                
                
                allState = {'tarcucmpub':status1A, 'tarcucm01': status1B, 'sbrcucm02': status1C, 'tarcucmjb01': status1D}
                
                for u in allState:
                    if allState[u][0] == 'fail':
                        if 'tarcucmpub' in allState[u]:
                            allState[u].append(request.POST["detail1A"])
                            statedate1 = request.POST["date1"]
                            info1A = voiceMPCR(dateStatus=statedate1, status=allState[u][0], hostname=allState[u][1], IP=allState[u][2], description=allState[u][3])
                            info1A.save()
                        if 'tarcucm01' in allState[u]:
                            allState[u].append(request.POST["detail1B"])
                            statedate1 = request.POST["date1"]
                            info1B = voiceMPCR(dateStatus=statedate1, status=allState[u][0], hostname=allState[u][1], IP=allState[u][2], description=allState[u][3])
                            info1B.save()
                        if 'sbrcucm02' in allState[u]:
                            allState[u].append(request.POST["detail1C"])
                            statedate1 = request.POST["date1"]
                            info1C = voiceMPCR(dateStatus=statedate1, status=allState[u][0], hostname=allState[u][1], IP=allState[u][2], description=allState[u][3])
                            info1C.save()
                        if 'tarcucmjb01' in allState[u]:
                            allState[u].append(request.POST["detail1D"])
                            statedate1 = request.POST["date1"]
                            info1C = voiceMPCR(dateStatus=statedate1, status=allState[u][0], hostname=allState[u][1], IP=allState[u][2], description=allState[u][3])
                            infoC.save()

                    if allState[u][0] == 'pass':
                        if 'tarcucmpub' in allState[u]:
                            statedate1 = request.POST["date1"]
                            info1AA = voiceMPCR(dateStatus=statedate1, status=allState[u][0], hostname=allState[u][1], IP=allState[u][2])
                            info1AA.save()
                        if 'tarcucm01' in allState[u]:
                            statedate1 = request.POST["date1"]
                            info1BB = voiceMPCR(dateStatus=statedate1, status=allState[u][0], hostname=allState[u][1], IP=allState[u][2])
                            info1BB.save()
                        if 'sbrcucm02' in allState[u]:
                            statedate1 = request.POST["date1"]
                            info1CC = voiceMPCR(dateStatus=statedate1, status=allState[u][0], hostname=allState[u][1], IP=allState[u][2])
                            info1CC.save()
                        if 'tarcucmjb01' in allState[u]:
                            statedate1 = request.POST["date1"]
                            info1DD = voiceMPCR(dateStatus=statedate1, status=allState[u][0], hostname=allState[u][1], IP=allState[u][2])
                            info1DD.save()
                    #print('state1 is finish.....')

                
                status2A = [request.POST["status2A"], hostname0, ip0]
                status2B = [request.POST["status2B"], hostname1, ip1]
                status2C = [request.POST["status2C"], hostname2, ip2]
                status2D = [request.POST["status2D"], hostname3, ip3]

                allState2 = {'tarcucmpub':status2A, 'tarcucm01': status2B, 'sbrcucm02': status2C, 'tarcucmjb01': status2D}    

                for state2 in allState2:

                    if allState2[state2][0] == 'fail':
                        if 'tarcucmpub' in allState2[state2]:
                            allState2[state2].append(request.POST["detail2A"])
                            #print(str(allState2))
                            statedate2 = request.POST["date2"]
                            info2A = voiceMCTIR(dateStatus=statedate2, status=allState2[state2][0], hostname=allState2[state2][1], IP=allState2[state2][2], description=allState2[state2][3])
                            info2A.save()
                        if 'tarcucm01' in allState2[state2]:
                            allState2[state2].append(request.POST["detail2B"])
                            statedate2 = request.POST["date2"]
                            info2B = voiceMCTIR(dateStatus=statedate2, status=allState2[state2][0], hostname=allState2[state2][1], IP=allState2[state2][2], description=allState2[state2][3])
                            info2B.save()
                        if 'sbrcucm02' in allState2[state2]:
                            allState2[state2].append(request.POST["detail2C"])
                            statedate2 = request.POST["date2"]
                            info2C = voiceMCTIR(dateStatus=statedate2, status=allState2[state2][0], hostname=allState2[state2][1], IP=allState2[state2][2], description=allState2[state2][3])
                            info2C.save()
                        if 'tarcucmjb01' in allState2[state2]:
                            allState2[state2].append(request.POST["detail2D"])
                            statedate2 = request.POST["date2"]
                            info2D = voiceMCTIR(dateStatus=statedate2, status=allState2[state2][0], hostname=allState2[state2][1], IP=allState2[state2][2], description=allState2[state2][3])
                            info2D.save()

                    if allState2[state2][0] == 'pass':
                        if 'tarcucmpub' in allState2[state2]:
                            statedate2 = request.POST["date2"]
                            info2AA = voiceMCTIR(dateStatus=statedate2, status=allState2[state2][0], hostname=allState2[state2][1], IP=allState2[state2][2])
                            info2AA.save()
                        if 'tarcucm01' in allState2[state2]:
                            statedate2 = request.POST["date2"]
                            info2BB = voiceMCTIR(dateStatus=statedate2, status=allState2[state2][0], hostname=allState2[state2][1], IP=allState2[state2][2])
                            info2BB.save()
                        if 'sbrcucm02' in allState2[state2]:
                            statedate2 = request.POST["date2"]
                            info2CC = voiceMCTIR(dateStatus=statedate2, status=allState2[state2][0], hostname=allState2[state2][1], IP=allState2[state2][2])
                            info2CC.save()
                        if 'tarcucmjb01' in allState2[state2]:
                            statedate2 = request.POST["date2"]
                            info2DD = voiceMCTIR(dateStatus=statedate2, status=allState2[state2][0], hostname=allState2[state2][1], IP=allState2[state2][2])
                            info2DD.save()



                
                status3A = [request.POST["status3A"], hostname0, ip0]
                status3B = [request.POST["status3B"], hostname1, ip1]
                status3C = [request.POST["status3C"], hostname2, ip2]
                status3D = [request.POST["status3D"], hostname3, ip3]

                allState3 = {'tarcucmpub':status3A, 'tarcucm01': status3B, 'sbrcucm02': status3C, 'tarcucmjb01': status3D}    

                for state3 in allState3:

                    if allState3[state3][0] == 'fail':
                        if 'tarcucmpub' in allState3[state3]:
                            allState3[state3].append(request.POST["detail3A"])
                            #print(str(allState2))
                            statedate3 = request.POST["date3"]
                            info3A = voiceMTSCC(dateStatus=statedate3, status=allState3[state3][0], hostname=allState3[state3][1], IP=allState2[state2][2], description=allState3[state3][3])
                            info3A.save()
                        if 'tarcucm01' in allState3[state3]:
                            allState3[state3].append(request.POST["detail3B"])
                            statedate3 = request.POST["date3"]
                            info3B = voiceMTSCC(dateStatus=statedate3, status=allState3[state3][0], hostname=allState3[state3][1], IP=allState3[state3][2], description=allState3[state3][3])
                            info3B.save()
                        if 'sbrcucm02' in allState3[state3]:
                            allState3[state3].append(request.POST["detail3C"])
                            statedate3 = request.POST["date3"]
                            info3C = voiceMTSCC(dateStatus=statedate3, status=allState3[state3][0], hostname=allState3[state3][1], IP=allState3[state3][2], description=allState3[state3][3])
                            info3C.save()
                        if 'tarcucmjb01' in allState3[state3]:
                            allState3[state3].append(request.POST["detail3D"])
                            statedate3 = request.POST["date3"]
                            info3D = voiceMTSCC(dateStatus=statedate3, status=allState3[state3][0], hostname=allState3[state3][1], IP=allState2[state2][2], description=allState2[state2][3])
                            info3D.save()

                    if allState3[state3][0] == 'pass':
                        if 'tarcucmpub' in allState3[state3]:
                            statedate3 = request.POST["date3"]
                            info3AA = voiceMTSCC(dateStatus=statedate3, status=allState3[state3][0], hostname=allState3[state3][1], IP=allState3[state3][2])
                            info3AA.save()
                        if 'tarcucm01' in allState3[state3]:
                            statedate3 = request.POST["date3"]
                            info3BB = voiceMTSCC(dateStatus=statedate3, status=allState3[state3][0], hostname=allState3[state3][1], IP=allState3[state3][2])
                            info3BB.save()
                        if 'sbrcucm02' in allState3[state3]:
                            statedate3 = request.POST["date3"]
                            info3CC = voiceMTSCC(dateStatus=statedate3, status=allState3[state3][0], hostname=allState3[state3][1], IP=allState3[state3][2])
                            info3CC.save()
                        if 'tarcucmjb01' in allState3[state3]:
                            statedate3 = request.POST["date3"]
                            info3DD = voiceMTSCC(dateStatus=statedate3, status=allState3[state3][0], hostname=allState3[state3][1], IP=allState3[state3][2])
                            info3DD.save()



                status4A = [request.POST["status4A"], hostname0, ip0]
                status4B = [request.POST["status4B"], hostname1, ip1]
                status4C = [request.POST["status4C"], hostname2, ip2]
                status4D = [request.POST["status4D"], hostname3, ip3]

                allState4 = {'tarcucmpub':status4A, 'tarcucm01': status4B, 'sbrcucm02': status4C, 'tarcucmjb01': status4D}    

                for state4 in allState4:

                    if allState4[state4][0] == 'fail':
                        if 'tarcucmpub' in allState4[state4]:
                            allState4[state4].append(request.POST["detail4A"])
                            #print(str(allState2))
                            statedate4 = request.POST["date4"]
                            info4A = voiceMCSCUCM(dateStatus=statedate4, status=allState4[state4][0], hostname=allState4[state4][1], IP=allState4[state4][2], description=allState4[state4][3])
                            info4A.save()
                        if 'tarcucm01' in allState4[state4]:
                            allState4[state4].append(request.POST["detail4B"])
                            statedate4 = request.POST["date4"]
                            info4B = voiceMCSCUCM(dateStatus=statedate4, status=allState4[state4][0], hostname=allState4[state4][1], IP=allState4[state4][2], description=allState4[state4][3])
                            info4B.save()
                        if 'sbrcucm02' in allState4[state4]:
                            allState4[state4].append(request.POST["detail4C"])
                            statedate4 = request.POST["date4"]
                            info4C = voiceMCSCUCM(dateStatus=statedate4, status=allState4[state4][0], hostname=allState4[state4][1], IP=allState4[state4][2], description=allState4[state4][3])
                            info4C.save()
                        if 'tarcucmjb01' in allState4[state4]:
                            allState4[state4].append(request.POST["detail4D"])
                            statedate4 = request.POST["date4"]
                            info4D = voiceMCSCUCM(dateStatus=statedate4, status=allState4[state4][0], hostname=allState4[state4][1], IP=allState4[state4][2], description=allState4[state4][3])
                            info4D.save()

                    if allState4[state4][0] == 'pass':
                        if 'tarcucmpub' in allState4[state4]:
                            statedate4 = request.POST["date4"]
                            info4AA = voiceMCSCUCM(dateStatus=statedate4, status=allState4[state4][0], hostname=allState4[state4][1], IP=allState4[state4][2])
                            info4AA.save()
                        if 'tarcucm01' in allState4[state4]:
                            statedate4 = request.POST["date4"]
                            info4BB = voiceMCSCUCM(dateStatus=statedate4, status=allState4[state4][0], hostname=allState4[state4][1], IP=allState4[state4][2])
                            info4BB.save()
                        if 'sbrcucm02' in allState4[state4]:
                            statedate4 = request.POST["date4"]
                            info4CC = voiceMCSCUCM(dateStatus=statedate4, status=allState4[state4][0], hostname=allState4[state4][1], IP=allState4[state4][2])
                            info4CC.save()
                        if 'tarcucmjb01' in allState4[state4]:
                            statedate4 = request.POST["date4"]
                            info4DD = voiceMCSCUCM(dateStatus=statedate4, status=allState4[state4][0], hostname=allState4[state4][1], IP=allState4[state4][2])
                            info4DD.save()



                status5A = [request.POST["status5A"], hostname0, ip0]
                status5B = [request.POST["status5B"], hostname1, ip1]
                status5C = [request.POST["status5C"], hostname2, ip2]
                status5D = [request.POST["status5D"], hostname3, ip3]

                allState5 = {'tarcucmpub':status5A, 'tarcucm01': status5B, 'sbrcucm02': status5C, 'tarcucmjb01': status5D}    

                for state5 in allState5:

                    if allState5[state5][0] == 'fail':
                        if 'tarcucmpub' in allState5[state5]:
                            allState5[state5].append(request.POST["detail5A"])
                            #print(str(allState2))
                            statedate5 = request.POST["date5"]
                            info5A = voiceMCRTM(dateStatus=statedate5, status=allState5[state5][0], hostname=allState5[state5][1], IP=allState5[state5][2], description=allState5[state5][3])
                            info5A.save()
                        if 'tarcucm01' in allState5[state5]:
                            allState5[state5].append(request.POST["detail5B"])
                            statedate5 = request.POST["date5"]
                            info5B = voiceMCRTM(dateStatus=statedate5, status=allState5[state5][0], hostname=allState5[state5][1], IP=allState5[state5][2], description=allState5[state5][3])
                            info5B.save()
                        if 'sbrcucm02' in allState5[state5]:
                            allState5[state5].append(request.POST["detail5C"])
                            statedate5 = request.POST["date5"]
                            info5C = voiceMCRTM(dateStatus=statedate5, status=allState5[state5][0], hostname=allState5[state5][1], IP=allState5[state5][2], description=allState5[state5][3])
                            info5C.save()
                        if 'tarcucmjb01' in allState5[state5]:
                            allState5[state5].append(request.POST["detail5D"])
                            statedate5 = request.POST["date5"]
                            info5D = voiceMCRTM(dateStatus=statedate5, status=allState5[state5][0], hostname=allState5[state5][1], IP=allState5[state5][2], description=allState5[state5][3])
                            info5D.save()

                    if allState5[state5][0] == 'pass':
                        if 'tarcucmpub' in allState5[state5]:
                            statedate5 = request.POST["date5"]
                            info5AA = voiceMCRTM(dateStatus=statedate5, status=allState5[state5][0], hostname=allState5[state5][1], IP=allState5[state5][2])
                            info5AA.save()
                        if 'tarcucm01' in allState5[state5]:
                            statedate5 = request.POST["date5"]
                            info5BB = voiceMCRTM(dateStatus=statedate5, status=allState5[state5][0], hostname=allState5[state5][1], IP=allState5[state5][2])
                            info5BB.save()
                        if 'sbrcucm02' in allState5[state5]:
                            statedate5 = request.POST["date5"]
                            info5CC = voiceMCRTM(dateStatus=statedate5, status=allState5[state5][0], hostname=allState5[state5][1], IP=allState5[state5][2])
                            info5CC.save()
                        if 'tarcucmjb01' in allState5[state5]:
                            statedate5 = request.POST["date5"]
                            info5DD = voiceMCRTM(dateStatus=statedate5, status=allState5[state5][0], hostname=allState5[state5][1], IP=allState5[state5][2])
                            info5DD.save()


                
                
                status6A = [request.POST["status6A"], hostname6, ip6]
                status6B = [request.POST["status6B"], hostname7, ip7]
                

                allState6 = {'gomeeting':status6A, 'gomeeting2': status6B}    

                for state6 in allState6:

                    if allState6[state6][0] == 'fail':
                        if 'gomeeting.cpall.co.th' in allState6[state6]:
                            allState6[state6].append(request.POST["detail6A"])
                            #print(str(allState2))
                            statedate6 = request.POST["date6"]
                            info6A = voiceMSTA(dateStatus=statedate6, status=allState6[state6][0], hostname=allState6[state6][1], IP=allState6[state6][2], description=allState6[state6][3])
                            info6A.save()
                        if 'gomeeting2.cpall.co.th' in allState6[state6]:
                            allState6[state6].append(request.POST["detail6B"])
                            statedate6 = request.POST["date6"]
                            info6B = voiceMSTA(dateStatus=statedate6, status=allState6[state6][0], hostname=allState6[state6][1], IP=allState6[state6][2], description=allState6[state6][3])
                            info6B.save()
                        

                    if allState6[state6][0] == 'pass':
                        if 'gomeeting.cpall.co.th' in allState6[state6]:
                            statedate6 = request.POST["date6"]
                            info6AA = voiceMSTA(dateStatus=statedate6, status=allState6[state6][0], hostname=allState6[state6][1], IP=allState6[state6][2])
                            info6AA.save()
                        if 'gomeeting2.cpall.co.th' in allState6[state6]:
                            statedate6 = request.POST["date6"]
                            info6BB = voiceMSTA(dateStatus=statedate6, status=allState6[state6][0], hostname=allState6[state6][1], IP=allState6[state6][2])
                            info6BB.save()
                        

            
            
            
            
            except Exception as e:
                print(e)
                

    context = {'entrydata': entrydata,
               'daytime': daytime,
              }
    
    return render(request,'voiceM.html', context)

# @app.route('/test', methods=['POST'])
# def test(request):
#     if request.method == 'POST':
#         if request.POST["cucmctiroute"]:
#             print("เงื่อนไขที่สอง")