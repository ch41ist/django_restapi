from django.shortcuts import render
import netmiko
import json
from datetime import datetime, timedelta
import time, threading
import schedule
import time
from checklist.voice.models import voicediskcapacitycript

times = 40

def voicedatacheckdisk():
    devices = """
        110.170.246.201
        110.170.246.202
        """.strip().splitlines()

    device_type = 'cisco_ios'
    username = 'root'
    password = 'CbcGosoftippbx9779'

    netmiko_exceptions = (netmiko.ssh_exception.NetMikoTimeoutException,
                          netmiko.ssh_exception.NetMikoAuthenticationException)
    
    for device in devices:
        try:
            print('/'*150)
            print('Connect to: ', device)
            dataTemp = []
            connect = netmiko.ConnectHandler(
                ip=device,
                device_type=device_type,
                username=username,
                password=password
            )
            sendCommandcheckdisk = connect.send_command("df -kh | grep /dev/mapper/SangomaVG-root")
            dataTemp = sendCommandcheckdisk.split(" ")
            diskCaps = dataTemp[10]
            castDatatoFloat = diskCaps.replace('%', '')
            dataChecker = float(castDatatoFloat)

            if(dataChecker <= 80.0):
                saveDatabasediskcapacity = voicediskcapacitycript(
                ip=device,
                workname='check disk status',
                descriptions='disk capacity',
                howto='ssh to server and type df -kh',
                status='Pass',
                diskused=dataChecker
                )
                saveDatabasediskcapacity.save()
                connect.disconnect()
            else:
                saveDatabasediskcapacity = voicediskcapacitycript(
                ip=device,
                workname='check disk status',
                descriptions='disk capacity',
                howto='ssh to server and type df -kh',
                status='Failed',
                diskused=dataChecker
                )
                saveDatabasediskcapacity.save()
                connect.disconnect()

        except netmiko_exceptions as e:
            print('Failed to ', device, e)
        
    threading.Timer(times, voicedatacheckdisk).start()

# voicedatacheckdisk()
