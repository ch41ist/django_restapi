from django.shortcuts import render
import netmiko
import json
from datetime import datetime, timedelta
import time, threading
import schedule
import time
from asteriskscript import voicedataasterisk 
from checkdiskused import voicedatacheckdisk
from models import voicedataautoscripts, voicedataautoscriptsHttpd, voicediskcapacitycript

schedule.every(10).seconds.do(voicedataasterisk)
schedule.every(10).seconds.do(voicedatacheckdisk)

while True: 
    schedule.run_pending() 
    time.sleep(1) 