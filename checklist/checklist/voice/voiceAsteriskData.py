from django.shortcuts import render
import netmiko
import json
from datetime import datetime, timedelta
import time, threading
import schedule
import time
import mysql.connector
import datetime
import subprocess

db = mysql.connector.connect(
    host = 'localhost',
    user = 'root',
    password = 'xcalculusx',
    database = 'checklistdata',
    auth_plugin='mysql_native_password'
)

daytime = datetime.datetime.now()
#times = 40

def voicedataasterisk():
    countas = 0
    devices = """
       10.151.36.105
       10.151.36.106
       10.151.36.107
       10.151.36.108
        """.strip().splitlines()

    device_type = 'cisco_ios'
    username = 'root'
    password = 'CbcGosoftippbx9779'

    netmiko_exceptions = (netmiko.ssh_exception.NetMikoTimeoutException,
                          netmiko.ssh_exception.NetMikoAuthenticationException)

    for device in devices:
        try:
            print('~' * 79)
            datlist = []
            print('Connecting to device:', device)
            connection = netmiko.ConnectHandler(ip=device, device_type=device_type, username=username, password=password)
            senthostname = connection.send_command("hostname")
            sendcommandasterisk = connection.send_command("service asterisk status | grep Active: ")
            hostname = senthostname
            filedatalist = sendcommandasterisk.split()
            ip = device
            workname = 'service asterisk status'
            descriptions = str(sendcommandasterisk)
            howto = 'running Script by web browser'
            status = filedatalist[1] + " " + filedatalist[2]
            uptime = filedatalist[8] + " " + filedatalist[9] + " " + filedatalist[10] + " " + filedatalist[11] + " " +filedatalist[12]
            created = daytime
            updated = daytime
            
            sendcommandahttpd = connection.send_command("service httpd status | grep Active: ")
            httpddatalist = sendcommandahttpd.split()
            #print(httpddatalist)
            worknamehttpd = 'service httpd status'
            descriptionshttpd = str(sendcommandahttpd)
            howtohttpd = 'running Script by web browser'
            httpdstatus = httpddatalist[6] + " " + httpddatalist[7]
            httpduptime = httpddatalist[13] + " " + httpddatalist[14] + " " + httpddatalist[15] + " " + httpddatalist[16] + " " + httpddatalist[17]

            print(httpdstatus)
            print(httpduptime)

            if (status == 'active (running)' and httpdstatus == 'active (running)'):
                
                cursor = db.cursor()
                sql  = 'insert into voice_voicedataautoscripts (ip, workname, descriptions, howto, status, uptime, created, updated, hostname) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s )'
                values = (device, workname, descriptions, howto, status, uptime, daytime, daytime, hostname)
                cursor.execute(sql, values)
                db.commit()
                print('process: Asterisk ทำงานเสร็จสิ้น...')

                cursor2 = db.cursor()
                sql2  = 'insert into voice_voicedataautoscriptshttpd (ip, workname, descriptions, howto, status, uptime, created, updated, hostname) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'
                values2 = (device, worknamehttpd, descriptionshttpd, howtohttpd, httpdstatus, httpduptime, daytime, daytime, hostname)
                cursor2.execute(sql2, values2)
                db.commit()
                print('process: HTTP ทำงานเสร็จสิ้น...')

        except netmiko_exceptions as e:
            print('ไม่สามารถเชื่อมต่อกับ: ', device, e)

            cursor = db.cursor()
            sql  = 'insert into voice_voicedataautoscripts (ip, workname, descriptions, howto, status, uptime, created, updated, hostname) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'
            values = (device, 'service asterisk status', 'unable to connect host', 'running Script by web browser', 'unable to connect host', 'unable to connect host', daytime, daytime, hostname)
            cursor.execute(sql, values)
            db.commit()
            print('process: Asterisk ทำงานเสร็จสิ้น...')

            cursor2 = db.cursor()
            sql2  = 'insert into voice_voicedataautoscriptshttpd (ip, workname, descriptions, howto, status, uptime, created, updated, hostname) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'
            values2 = (device, 'service httpd status', 'unable to connect host', 'running Script by web browser', 'unable to connect host', 'unable to connect host', daytime, daytime, hostname)
            cursor2.execute(sql2, values2)
            db.commit()
            print('process: HTTP ทำงานเสร็จสิ้น...')
    #     countas += 1
    # print(countas)


def voicedatacheckdisk():
    countdsk = 0
    devices = """
        
        10.151.36.105
        10.151.36.106
        10.151.36.107
        10.151.36.108
        """.strip().splitlines()

    device_type = 'cisco_ios'
    username = 'root'
    password = 'CbcGosoftippbx9779'

    netmiko_exceptions = (netmiko.ssh_exception.NetMikoTimeoutException,
                          netmiko.ssh_exception.NetMikoAuthenticationException)
    
    for device in devices:
        try:
            print('~'*79)
            print('Connect to: ', device)
            #dataTemp = []
            connect = netmiko.ConnectHandler(
                ip=device,
                device_type=device_type,
                username=username,
                password=password
            )
            sendCommandcheckdisk = connect.send_command("df -kh | grep /dev/mapper/SangomaVG-root")
            hostname = connect.send_command("hostname")
            diskdatalist = sendCommandcheckdisk.split()
            ip = device
            #print(diskdatalist)
            size = diskdatalist[1].replace('G', '')
            used = diskdatalist[2].replace('G', '')
            diskused = diskdatalist[4].replace('%', '')
            dataChecker = int(diskused)

            if(dataChecker == 100):

                cursor = db.cursor()
                sql  = 'insert into voice_voicediskcapacitycript (ip, workname, descriptions, howto, status, diskused, created, updated, hostname, size, used) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                values = (device, 'check disk status', 'disk capacity', 'ssh to server and type df -kh', 'Failed', diskused, daytime, daytime, hostname, size, used)
                cursor.execute(sql, values)
                db.commit()
                print('process: Check Disk Capacity มีปัญหา!!! ตรวจพบปริมาญดิสก์เต็ม')

            elif(dataChecker >= 80 and dataChecker <= 99):
                cursor = db.cursor()
                sql  = 'insert into voice_voicediskcapacitycript (ip, workname, descriptions, howto, status, diskused, created, updated, hostname, size, used) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                values = (device, 'check disk status', 'disk capacity', 'ssh to server and type df -kh', 'Alert', diskused, daytime, daytime, hostname, size, used)
                cursor.execute(sql, values)
                db.commit()
                print('process: Check Disk Capacity แจ้งเตือนพื้นที่เหลือน้อยเกินกว่าที่กำหนดกรุณาตรวจสอบ service ดังกล่าว...กำลังใช้พื้นที่ทั้งหมด: '+diskused+'%')
            else:
                cursor = db.cursor()
                sql  = 'insert into voice_voicediskcapacitycript (ip, workname, descriptions, howto, status, diskused, created, updated, hostname, size, used) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                values = (device, 'check disk status', 'disk capacity', 'ssh to server and type df -kh', 'Pass', diskused, daytime, daytime, hostname, size, used)
                cursor.execute(sql, values)
                db.commit()
                print('process: Check Disk Capacity ทำงานเสร็จสิ้น...กำลังใช้พื้นที่ทั้งหมด: '+diskused+ '%')

        except netmiko_exceptions as e:
            print('ไม่สามารถเชื่อมต่อกับ: ', device, e)
            cursor = db.cursor()
            sql  = 'insert into voice_voicediskcapacitycript (ip, workname, descriptions, howto, status, diskused, created, updated, hostname, size, used) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
            values = (device, 'check disk status', 'unable to connect host', 'ssh to server and type df -kh', 'unable to connect host', 'unable to connect host', daytime, daytime, hostname, 'unable to connect host', 'unable to connect host')
            cursor.execute(sql, values)
            db.commit()
            print('process: Check Disk Capacity ทำงานเสร็จสิ้น...')

        countdsk += 1
    print(countdsk)

voicedatacheckdisk()
# schedule.every(30).seconds.do(voicedataasterisk)
# schedule.every(30).seconds.do(voicedatacheckdisk)
# schedule.every(30).seconds.do(pingrouter)

# while True: 
#     schedule.run_pending() 
#     time.sleep(1) 