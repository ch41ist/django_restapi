from django.db import models
import datetime
from datetime import datetime
from django.utils import timezone

class voicedataautoscripts(models.Model):
    workname = models.CharField(max_length=200, default="")
    descriptions = models.CharField(max_length=1000, default="")
    ip = models.CharField(max_length=1000, default="")
    howto = models.CharField(max_length=1000, default="")
    status = models.CharField(max_length=200, default="")
    uptime = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)
    #starttime = models.CharField(max_length=200, default="")
    hostname = models.CharField(max_length=200, default="")

class voicedataautoscriptsHttpd(models.Model):
    workname = models.CharField(max_length=200, default="")
    descriptions = models.CharField(max_length=1000, default="")
    ip = models.CharField(max_length=1000, default="")
    howto = models.CharField(max_length=1000, default="")
    status = models.CharField(max_length=200, default="")
    uptime = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)
    #starttime = models.CharField(max_length=200, default="")
    hostname = models.CharField(max_length=200, default="")

class voicediskcapacitycript(models.Model):
    workname = models.CharField(max_length=200, default="")
    descriptions = models.CharField(max_length=1000, default="")
    ip = models.CharField(max_length=1000, default="")
    howto = models.CharField(max_length=1000, default="")
    status = models.CharField(max_length=200, default="")
    used = models.CharField(max_length=200, default="")
    size = models.CharField(max_length=200, default="")
    diskused = models.IntegerField(default=0)
    #uptime = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)
    hostname = models.CharField(max_length=200, default="")

class voiceMPCR(models.Model): ###Cisco CUCM port channel register###
    hostname = models.CharField(max_length=200, default="")
    IP = models.CharField(max_length=200, default="")
    status = models.CharField(max_length=200, default="")
        
    description = models.CharField(max_length=1000, default="")
    checklister = models.CharField(max_length=200, default="")
    dateStatus = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class voiceMCTIR(models.Model): ###Cisco CUCM CTI Route Point register###
    hostname = models.CharField(max_length=200, default="")
    IP = models.CharField(max_length=200, default="")
    status = models.CharField(max_length=200, default="")
        
    description = models.CharField(max_length=1000, default="")
    checklister = models.CharField(max_length=200, default="")
    dateStatus = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)

class voiceMTSCC(models.Model): ###Trunk Status Cisco CUCM###
    hostname = models.CharField(max_length=200, default="")
    IP = models.CharField(max_length=200, default="")
    status = models.CharField(max_length=200, default="")
        
    description = models.CharField(max_length=1000, default="")
    checklister = models.CharField(max_length=200, default="")
    dateStatus = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)



class voiceMCSCUCM(models.Model): ###Check Service CUCM###
    hostname = models.CharField(max_length=200, default="")
    IP = models.CharField(max_length=200, default="")
    status = models.CharField(max_length=200, default="")
        
    description = models.CharField(max_length=1000, default="")
    checklister = models.CharField(max_length=200, default="")
    dateStatus = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)


class voiceMCRTM(models.Model): ###Cisco Real-Time monitoring###
    hostname = models.CharField(max_length=200, default="")
    IP = models.CharField(max_length=200, default="")
    status = models.CharField(max_length=200, default="")
        
    description = models.CharField(max_length=1000, default="")
    checklister = models.CharField(max_length=200, default="")
    dateStatus = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)


class voiceMSTA(models.Model): ###Openmeeting ctusheck sta###
    hostname = models.CharField(max_length=200, default="")
    IP = models.CharField(max_length=200, default="")
    status = models.CharField(max_length=200, default="")
        
    description = models.CharField(max_length=1000, default="")
    checklister = models.CharField(max_length=200, default="")
    dateStatus = models.CharField(max_length=200, default="")
    created = models.DateField(auto_now_add=True, null=True, blank=True)
    updated = models.DateField(auto_now=True, null=True, blank=True)