from django.shortcuts import render
import mysql.connector
from datetime import date
from checklist.sdwan.models import edges, links, alldata, dataalert
from checklist.wirelessoffice.models import wireofficeDown, controllerhealthCPU, controllerhealthRAM, countap
from checklist.voice.models import voicediskcapacitycript, voicedataautoscripts, voicedataautoscriptsHttpd
from checklist.voice.models import voicediskcapacitycript , voicedataautoscriptsHttpd
from checklist.voice.models import voicedataautoscripts
from checklist.f5.models import f5Fan, f5PSU, f5MB

# db = mysql.connector.connect(
#     host = 'localhost',
#     user = 'root',
#     password = 'xcalculusx',
#     database = 'voice_runningscript'
# )

def dashboard(request):
    edge = edges.objects.filter().order_by('-id')[0]
    link = links.objects.filter().order_by('-id')[0]
    allstate = alldata.objects.filter().order_by('-id')[:43]
    dataAlert = dataalert.objects.filter().latest('id')
    countAP  = countap.objects.filter().order_by('-id')[:5]
    diskcapstatus = voicediskcapacitycript.objects.filter().order_by('-id')[:4]
    asteriskstatus = voicedataautoscripts.objects.filter().order_by('-id')[:4]
    httpdstatus = voicedataautoscriptsHttpd.objects.filter().order_by('-id')[:4]
    voice_disk_cap_for_IP1 = voicediskcapacitycript.objects.filter().order_by('-id')[:4]
    voice_asterisk_server = voicedataautoscripts.objects.filter().order_by('-id')[:4]
    voice_httpd_server = voicedataautoscriptsHttpd.objects.filter().order_by('-id')[:4]
    healthcpu = controllerhealthCPU.objects.filter().order_by('-id')[:5]
    healthram = controllerhealthRAM.objects.filter().order_by('-id')[:5]
    f5fan = f5Fan.objects.filter().order_by('-id')[:10]
    f5psu = f5PSU.objects.filter().order_by('-id')[:10]
    f5mb = f5MB.objects.filter().order_by('-id')[:10]
    context = {'diskcapstatus': diskcapstatus,
               'asteriskstatus': asteriskstatus,
               'httpdstatus': httpdstatus,
               'edge': edge,
               'link': link,
               'allstate': allstate,
               'dataAlert': dataAlert,
               'countAP' : countAP,
               'voice_disk_cap_for_IP1': voice_disk_cap_for_IP1,
               'voice_asterisk_server': voice_asterisk_server,
               'voice_httpd_server': voice_httpd_server,
               'healthcpu': healthcpu,
               'healthram': healthram,
               'f5fan': f5fan,
               'f5psu': f5psu,
               'f5mb': f5mb,
              }
    return render(request,'dashboard.html', context)
