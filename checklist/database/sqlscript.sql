-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema voice_runningscript
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema voice_runningscript
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `voice_runningscript` DEFAULT CHARACTER SET latin1 ;
USE `voice_runningscript` ;

-- -----------------------------------------------------
-- Table `voice_runningscript`.`auth_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`auth_group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`django_content_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`django_content_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `app_label` VARCHAR(100) NOT NULL,
  `model` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label` ASC, `model` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`auth_permission`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`auth_permission` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `content_type_id` INT(11) NOT NULL,
  `codename` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id` ASC, `codename` ASC),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co`
    FOREIGN KEY (`content_type_id`)
    REFERENCES `voice_runningscript`.`django_content_type` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 49
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`auth_group_permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`auth_group_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `group_id` INT(11) NOT NULL,
  `permission_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id` ASC, `permission_id` ASC),
  INDEX `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id` ASC),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm`
    FOREIGN KEY (`permission_id`)
    REFERENCES `voice_runningscript`.`auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `voice_runningscript`.`auth_group` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`auth_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`auth_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(128) NOT NULL,
  `last_login` DATETIME(6) NULL DEFAULT NULL,
  `is_superuser` TINYINT(1) NOT NULL,
  `username` VARCHAR(150) NOT NULL,
  `first_name` VARCHAR(30) NOT NULL,
  `last_name` VARCHAR(150) NOT NULL,
  `email` VARCHAR(254) NOT NULL,
  `is_staff` TINYINT(1) NOT NULL,
  `is_active` TINYINT(1) NOT NULL,
  `date_joined` DATETIME(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username` (`username` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`auth_user_groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`auth_user_groups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `group_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id` ASC, `group_id` ASC),
  INDEX `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id` ASC),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `voice_runningscript`.`auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `voice_runningscript`.`auth_user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`auth_user_user_permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`auth_user_user_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `permission_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id` ASC, `permission_id` ASC),
  INDEX `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id` ASC),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm`
    FOREIGN KEY (`permission_id`)
    REFERENCES `voice_runningscript`.`auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `voice_runningscript`.`auth_user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`background_task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`background_task` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `task_name` VARCHAR(190) NOT NULL,
  `task_params` LONGTEXT NOT NULL,
  `task_hash` VARCHAR(40) NOT NULL,
  `verbose_name` VARCHAR(255) NULL DEFAULT NULL,
  `priority` INT(11) NOT NULL,
  `run_at` DATETIME(6) NOT NULL,
  `repeat` BIGINT(20) NOT NULL,
  `repeat_until` DATETIME(6) NULL DEFAULT NULL,
  `queue` VARCHAR(190) NULL DEFAULT NULL,
  `attempts` INT(11) NOT NULL,
  `failed_at` DATETIME(6) NULL DEFAULT NULL,
  `last_error` LONGTEXT NOT NULL,
  `locked_by` VARCHAR(64) NULL DEFAULT NULL,
  `locked_at` DATETIME(6) NULL DEFAULT NULL,
  `creator_object_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  `creator_content_type_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `background_task_creator_content_type_61cc9af3_fk_django_co` (`creator_content_type_id` ASC),
  INDEX `background_task_task_name_4562d56a` (`task_name` ASC),
  INDEX `background_task_task_hash_d8f233bd` (`task_hash` ASC),
  INDEX `background_task_priority_88bdbce9` (`priority` ASC),
  INDEX `background_task_run_at_7baca3aa` (`run_at` ASC),
  INDEX `background_task_queue_1d5f3a40` (`queue` ASC),
  INDEX `background_task_attempts_a9ade23d` (`attempts` ASC),
  INDEX `background_task_failed_at_b81bba14` (`failed_at` ASC),
  INDEX `background_task_locked_by_db7779e3` (`locked_by` ASC),
  INDEX `background_task_locked_at_0fb0f225` (`locked_at` ASC),
  CONSTRAINT `background_task_creator_content_type_61cc9af3_fk_django_co`
    FOREIGN KEY (`creator_content_type_id`)
    REFERENCES `voice_runningscript`.`django_content_type` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`background_task_completedtask`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`background_task_completedtask` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `task_name` VARCHAR(190) NOT NULL,
  `task_params` LONGTEXT NOT NULL,
  `task_hash` VARCHAR(40) NOT NULL,
  `verbose_name` VARCHAR(255) NULL DEFAULT NULL,
  `priority` INT(11) NOT NULL,
  `run_at` DATETIME(6) NOT NULL,
  `repeat` BIGINT(20) NOT NULL,
  `repeat_until` DATETIME(6) NULL DEFAULT NULL,
  `queue` VARCHAR(190) NULL DEFAULT NULL,
  `attempts` INT(11) NOT NULL,
  `failed_at` DATETIME(6) NULL DEFAULT NULL,
  `last_error` LONGTEXT NOT NULL,
  `locked_by` VARCHAR(64) NULL DEFAULT NULL,
  `locked_at` DATETIME(6) NULL DEFAULT NULL,
  `creator_object_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  `creator_content_type_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `background_task_comp_creator_content_type_21d6a741_fk_django_co` (`creator_content_type_id` ASC),
  INDEX `background_task_completedtask_task_name_388dabc2` (`task_name` ASC),
  INDEX `background_task_completedtask_task_hash_91187576` (`task_hash` ASC),
  INDEX `background_task_completedtask_priority_9080692e` (`priority` ASC),
  INDEX `background_task_completedtask_run_at_77c80f34` (`run_at` ASC),
  INDEX `background_task_completedtask_queue_61fb0415` (`queue` ASC),
  INDEX `background_task_completedtask_attempts_772a6783` (`attempts` ASC),
  INDEX `background_task_completedtask_failed_at_3de56618` (`failed_at` ASC),
  INDEX `background_task_completedtask_locked_by_edc8a213` (`locked_by` ASC),
  INDEX `background_task_completedtask_locked_at_29c62708` (`locked_at` ASC),
  CONSTRAINT `background_task_comp_creator_content_type_21d6a741_fk_django_co`
    FOREIGN KEY (`creator_content_type_id`)
    REFERENCES `voice_runningscript`.`django_content_type` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`django_admin_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`django_admin_log` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `action_time` DATETIME(6) NOT NULL,
  `object_id` LONGTEXT NULL DEFAULT NULL,
  `object_repr` VARCHAR(200) NOT NULL,
  `action_flag` SMALLINT(5) UNSIGNED NOT NULL,
  `change_message` LONGTEXT NOT NULL,
  `content_type_id` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id` ASC),
  INDEX `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id` ASC),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co`
    FOREIGN KEY (`content_type_id`)
    REFERENCES `voice_runningscript`.`django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `voice_runningscript`.`auth_user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`django_migrations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`django_migrations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `app` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `applied` DATETIME(6) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 39
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`django_session`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`django_session` (
  `session_key` VARCHAR(40) NOT NULL,
  `session_data` LONGTEXT NOT NULL,
  `expire_date` DATETIME(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  INDEX `django_session_expire_date_a5c62663` (`expire_date` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`voice_voicedataautoscripts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`voice_voicedataautoscripts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `descriptions` VARCHAR(1000) NOT NULL,
  `ip` VARCHAR(1000) NOT NULL,
  `howto` VARCHAR(1000) NOT NULL,
  `status` VARCHAR(200) NOT NULL,
  `created` DATE NULL DEFAULT NULL,
  `updated` DATE NULL DEFAULT NULL,
  `workname` VARCHAR(200) NOT NULL,
  `uptime` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 375
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`voice_voicedataautoscriptshttpd`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`voice_voicedataautoscriptshttpd` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `workname` VARCHAR(200) NOT NULL,
  `descriptions` VARCHAR(1000) NOT NULL,
  `ip` VARCHAR(1000) NOT NULL,
  `howto` VARCHAR(1000) NOT NULL,
  `status` VARCHAR(200) NOT NULL,
  `uptime` VARCHAR(200) NOT NULL,
  `created` DATE NULL DEFAULT NULL,
  `updated` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 241
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `voice_runningscript`.`voice_voicediskcapacitycript`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `voice_runningscript`.`voice_voicediskcapacitycript` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `workname` VARCHAR(200) NOT NULL,
  `descriptions` VARCHAR(1000) NOT NULL,
  `ip` VARCHAR(1000) NOT NULL,
  `howto` VARCHAR(1000) NOT NULL,
  `status` VARCHAR(200) NOT NULL,
  `created` DATE NULL DEFAULT NULL,
  `updated` DATE NULL DEFAULT NULL,
  `diskused` INT(10) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 93
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
